import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import {IndexComponent} from './components/index/index.component'
import { LoginComponent } from './components/login/login.page';
import { UserMenuComponent } from './components/user/userMenu.component';
import { PasswordRecoveryComponent } from './components/passwordRecovery/passwordRecovery.page';
import { EditUserPasswordComponent } from './components/editUserPassword/editUserPassword.page';
import { TableComponent } from './components/tables/table.component';
import { ConfigComponent } from './components/config/config.component';
import { ProductCreateComponent } from './components/inventory/products/product_create/product_create.component';
import { ProductPriceComponent } from './components/inventory/products/productPrice/productPrice.component';
import { InventoryMenuComponent } from './components/inventory/inventoryMenu.component';
import { ProductManagmentComponent} from './components/inventory/products/product_managment/product_managment.component';
import { ProviderManagmentComponent} from './components/inventory/providers/provider_managment/provider_managment.component';
import { CategoriesManagmentComponent} from './components/inventory/categories/categories_managment/categories_managment.component';
import { RecipeComponent} from './components/inventory/products/recipe/recipe.component';
import { ModifiersComponent} from './components/inventory/products/modifiers/modifiers.component';
import { SuggestedComponent} from './components/inventory/products/suggested/suggested.component';
import { InventoryItemComponent } from './components/inventory/inventory/inventoryItem/inventoryItem.component';
import { InventoryItemEntryComponent } from './components/inventory/inventory/inventoryItemEntry/inventoryItemEntry.component';
import { InventoryItemPriceComponent } from './components/inventory/inventory/inventoryItemPrices/inventoryItemPrice.component';
import { InventoryItemOutputComponent } from './components/inventory/inventory/inventoryItemOutput/inventoryItemOutput.component';
import { InventoryItemMovementsComponent } from './components/inventory/inventory/inventoryItemMovements/inventoryItemMovements.component';
import { CreatePromotionsComponent } from './components/inventory/promotions/createPromotions/createPromotions.component';
import { EditPromotionsComponent } from './components/inventory/promotions/editPromotions/editPromotions.component';

const routes: Routes = [
  {
    path: 'login',
    // loadChildren: './components/login/login.module#LoginPageModule',
    component: LoginComponent,
    pathMatch: 'full',
    data: {
      name: 'Login'
    }
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  // {
  //   path: '**',
  //   redirectTo: 'login',
  //   pathMatch: 'full',
  // },
  {
    path: 'home',
    component: IndexComponent,
    pathMatch: 'full',
  },
  {
    path: 'user',
    component: UserMenuComponent,
    pathMatch: 'full',
  },
  {
    path: 'userChangePassword/tokenVerification/:uuid',
    component: EditUserPasswordComponent,
    pathMatch: 'full',
  },
  {
    path: 'passwordRecovery',
    component: PasswordRecoveryComponent,
    pathMatch: 'full',
  },
  {
    path: 'table',
    component: TableComponent,
    pathMatch: 'full',
  },
  {
    path: 'inventory',
    component: InventoryMenuComponent,
    pathMatch: 'full',
  },
  {
    path: 'inventory/item/:id',
    component: InventoryItemComponent,
    pathMatch: 'full',
  },
  {
    path: 'inventory/item',
    component: InventoryItemComponent,
    pathMatch: 'full',
  },
  {
    path: 'inventory/item/entry/:id',
    component: InventoryItemEntryComponent,
    pathMatch: 'full',
  },
  // {
    //   path: 'product/item/price/:id',
    //   component: InventoryItemPriceComponent,
    //   pathMatch: 'full',
    // },
    {
      path: 'inventory/item/output/:id',
      component: InventoryItemOutputComponent,
      pathMatch: 'full',
    },
    {
      path: 'inventory/item/movements/:id',
      component: InventoryItemMovementsComponent,
      pathMatch: 'full',
    },
    {
      path: 'config',
      component: ConfigComponent,
      pathMatch: 'full',
    },
    {
      path: 'product',
      component: ProductCreateComponent,
      pathMatch: 'full',
    },
    {
      path: 'product/:id',
      component: ProductCreateComponent,
      pathMatch: 'full',
    },
    {
      path: 'product/price/:id',
      component: ProductPriceComponent,
      pathMatch: 'full',
    },
    {
      path: 'provider',
      component: ProviderManagmentComponent,
      pathMatch: 'full',
    },
    {
      path: 'provider/:id',
      component: ProviderManagmentComponent,
      pathMatch: 'full',
    },
    {
      path: 'category',
      component: CategoriesManagmentComponent,
      pathMatch: 'full',
    },
    {
      path: 'category/:id',
      component: CategoriesManagmentComponent,
      pathMatch: 'full',
    },
    {
      path: 'recipe',
      component: RecipeComponent,
      pathMatch: 'full',
    },
    {
      path: 'recipe/:id',
      component: RecipeComponent,
      pathMatch: 'full',
    },
    {
      path: 'modifiers',
      component: ModifiersComponent,
      pathMatch: 'full',
    },
    {
      path: 'modifiers/:id',
      component: ModifiersComponent,
      pathMatch: 'full',
    },
    {
      path: 'suggested',
      component: SuggestedComponent,
      pathMatch: 'full',
    },
    {
      path: 'suggested/:id',
      component: SuggestedComponent,
      pathMatch: 'full',
    },
    {
      path: 'promotion',
      component: CreatePromotionsComponent,
      pathMatch: 'full',
    },
    {
      path: 'promotion/:id',
      component: EditPromotionsComponent,
      pathMatch: 'full',
    },
  ];
  
  @NgModule({
    imports: [
      RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
