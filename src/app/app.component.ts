import { Component, ElementRef} from '@angular/core';
import { LocalConfigService } from './services/localConfig.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  preserveWhitespaces: true, // Needed for white-space around links.
  host: {
		"[class.light-theme]": `( theme === 'light' )`,
		"[class.dark-theme]": `( theme === 'dark' )`
  },
})
export class AppComponent {
  title = 'Pospay';

  public theme: string;

  
  constructor(private elementRef: ElementRef) {
    this.theme = "light";
  }

  



}
