import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';

import { IndexModule } from './components/index/index.module';
import { UserMenuModule } from './components/user/userMenu.module';
import { TableModule } from './components/tables/table.module';
import { ConfigModule } from './components/config/config.module';
import { InventoryMenuModule } from './components/inventory/inventoryMenu.module';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';

import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import { LoginComponent } from './components/login/login.page';
import { PasswordRecoveryComponent } from './components/passwordRecovery/passwordRecovery.page';
import { EditUserPasswordComponent } from './components/editUserPassword/editUserPassword.page';
import { IndexComponent } from './components/index/index.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import { MatFormFieldModule, MatSelectModule } from '@angular/material';
import {  MatInputModule } from '@angular/material';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PasswordRecoveryComponent,
    EditUserPasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,  MatInputModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatSidenavModule,
    MatGridListModule,
    MatTableModule,
    HttpClientModule,
    MatPaginatorModule,
    MatIconModule,
    MatDialogModule,
    MatDividerModule,
    MatButtonModule,
    MatSnackBarModule,
    MatTooltipModule,
    IndexModule,
    UserMenuModule,
    MatProgressBarModule,
    NgbModule,
    TableModule,
    ConfigModule,
    InventoryMenuModule,
    MatSelectModule,
    MatFormFieldModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
