import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'delete-dialog',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteDialogComponent implements OnInit{
  title = "";
  buttonText = "";
  contentText = "";

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<DeleteDialogComponent>,
    private _snackBar: MatSnackBar,
  ){
  }

  async ngOnInit() {
    this.title = this.data.title;
    this.buttonText = this.data.buttonText;
    this.contentText = this.data.contentText;
  }

  closeDialog(action){
    this.dialogRef.close(action);
  }

}
