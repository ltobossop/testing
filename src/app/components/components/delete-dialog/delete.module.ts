import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DeleteDialogComponent } from './delete.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    DeleteDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
  ],
  providers: [],
  bootstrap: [DeleteDialogComponent],
  exports:[DeleteDialogComponent]
})
export class DeleteDialogModule { }
