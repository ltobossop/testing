import { Component, OnInit, ViewChild, Inject,Input } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-title-bar',
  templateUrl: './title-bar.component.html',
  styleUrls: ['./title-bar.component.css'],
})



export class TitleBarComponent implements OnInit {
  @Input() title = "Loading";

  constructor(){
  }

  async ngOnInit() {
  }

  ngAfterViewInit() {
  }

}