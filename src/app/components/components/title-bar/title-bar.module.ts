import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Component} from '@angular/core';
import { RouterModule } from '@angular/router';


import { TitleBarComponent } from './title-bar.component';


@NgModule({
  declarations: [
    TitleBarComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule
  ],
  exports: [TitleBarComponent],
  bootstrap: [TitleBarComponent],
})
export class TitleBarModule { }