import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { CompanyService } from '../../../services/company.service'

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class CompanyComponent implements OnInit {

  companyInfo: FormGroup;
  data;
  isEdit = false;

  constructor(
    private formBuilder: FormBuilder,
    private companyService: CompanyService
  ){
    this.starForm();
  }

  async ngOnInit() {

    await this.loadCompanyInfo();
  

  }

  async loadCompanyInfo(){
    await this.companyService.getInfo().subscribe((result:any)=>{
      this.data = result[0];
      if(this.data.commercialName){
        console.log(this.data)
        this.loadForm();
        this.isEdit = true;
      }
      else
        this.starForm();
    });
  }

  starForm(){
    this.companyInfo = this.formBuilder.group({
      commercialName: ['' ,  [Validators.required]],
      commercialAddress: ['' ,  [Validators.required]],
      commercialSuburb: ['' ,  [Validators.required]],
      commercialPostalCode: ['', [Validators.required]],
      commercialCity: ['', [Validators.required]],
      commercialState: ['', [Validators.required]],
      commercialPhone: ['', [Validators.required]],
      commercialPhoneSecondary: ['', [Validators.required]],
      
      mtfOpenFrom: ['', [Validators.required]],
      mtfOpenTo: ['', [Validators.required]],
      saturdayOpenFrom: ['', [Validators.required]],
      saturdayOpenTo: ['', [Validators.required]],
      sundayOpenFrom: ['', [Validators.required]],
      sundayOpenTo: ['', [Validators.required]],
      
      bussinessName: ['', [Validators.required]],
      rfc: ['', [Validators.required]],
      address: ['', [Validators.required]],
      regime: ['', [Validators.required]],
      suburb: ['', [Validators.required]],
      postalCode: ['', [Validators.required]],
      commercialPhoneF: ['', [Validators.required]],
      commercialPhoneSecondaryF: ['', [Validators.required]],
    });
  }

  async loadForm(){
    
    this.companyInfo.patchValue({
      commercialName: this.data.commercialName,
      commercialAddress: this.data.commercialAddress,
      commercialSuburb: this.data.commercialSuburb,
      commercialPostalCode: this.data.commercialPostalCode,
      commercialCity: this.data.commercialCity,
      commercialState: this.data.commercialState,
      commercialPhone: this.data.commercialPhone,
      commercialPhoneSecondary: this.data.commercialPhoneSecondary,
      
      mtfOpenFrom: this.data.mtfOpenFrom,
      mtfOpenTo: this.data.mtfOpenTo,
      saturdayOpenFrom: this.data.saturdayOpenFrom,
      saturdayOpenTo: this.data.saturdayOpenTo,
      sundayOpenFrom: this.data.sundayOpenFrom,
      sundayOpenTo: this.data.sundayOpenTo,
      
      bussinessName: this.data.bussinessName,
      rfc: this.data.rfc,
      address: this.data.address,
      regime: this.data.regime,
      suburb: this.data.suburb,
      postalCode: this.data.postalCode,
      commercialPhoneF: this.data.commercialPhoneF,
      commercialPhoneSecondaryF: this.data.commercialPhoneSecondaryF,
    });
  }

  async saveInfo(){
    let a = this.companyInfo.value;
    a.daysOpened = "L";
    if(this.isEdit){
      a.id = this.data.id;
      await this.companyService.updateCompany(a).subscribe((result) =>{
        console.log(result)
      });
    }else{
      await this.companyService.saveCompany(a).subscribe((result) =>{
        console.log(result)
      });
    }
  }

  dayToggle(day){
    console.log(day)
  }

}