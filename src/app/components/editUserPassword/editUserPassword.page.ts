import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UsersService } from '../../services/user.service'

@Component({
  selector: 'app-editUserPassword',
  templateUrl: './editUserPassword.page.html',
  styleUrls: ['./editUserPassword.page.css']
})
export class EditUserPasswordComponent implements OnInit {

  isDisabled = false
  item;
  uuid;
  user;

  constructor(
    private userService: UsersService,
    private router: Router,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
  ){ }

  async ngOnInit() {
    this.verifyTokenUser();
    
    this.item = this.formBuilder.group({
      password: ['' ,  [Validators.required]],
      confirmPassword: ['' ],
    });
  }

  async verifyTokenUser(){
    await this.route.params.subscribe(params => {
      this.findUserUUid(params['uuid'])
    });
  }

  async findUserUUid(uuid){
    let data = {
      "uuid":uuid
    };
    this.uuid = uuid;
    this.userService.findByUUIDRecoveryPassword(data).subscribe((result:any)=>{
      if(result){
        this.user = result;
      }else{
        this.onError("El token ya no es valido, genere otro.")
      }
    },(error:any)=>{
      this.onError("El token ya no es valido, genere otro.")
    });

  }

  onError(msg){
    this.router.navigate(['/login'])
    this.openSnackBar(msg,"OK")
  }

  saveNewPassword(){

    if(this.item.value.password ===  this.item.value.confirmPassword){
      const data = {
        "id":this.user.id,
        "resetPasswordToken": this.uuid,
        "password":this.item.value.password
      }

      this.userService.changePasswordToken(data).subscribe((result:any)=>{
        this.router.navigate(['/login'])
        this.openSnackBar("Contraseña cambiada exitosamente","OK")
      },(error:any)=>{
        this.isDisabled = false;
        this.openSnackBar("Error, los datos son inválidos","OK")
      });

    }else{
      this.isDisabled = false;
      this.openSnackBar("Error, los datos son inválidos","OK")
    }
  }

  async loadInfo(){

    const data = {
      "email": this.item.value.email
    }
    
    this.userService.sendEmailRecoveryPassword(data).subscribe((result:any)=>{
      this.router.navigate(['/login'])
      this.openSnackBar("Se envió correctamente el correo, favor de verificar en tu bandeja de entrada","OK")
    },(error:any)=>{
      this.isDisabled = false;
      this.openSnackBar("Error, los datos son inválidos","OK")
    });

  }

  goLogin(){
    this.router.navigate(['/login'])
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  async showLoading() {
    this.isDisabled = true;
  }

}
