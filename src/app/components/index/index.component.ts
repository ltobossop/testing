import { Component, OnInit, ViewChild,ElementRef, Renderer2 } from '@angular/core';
import { CompanyService } from '../../services/company.service'
import { UsersService } from '../../services/user.service'
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Router} from "@angular/router"
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})



export class IndexComponent implements OnInit {
  displayedColumns;
  companyInfo;
  backgroundURL;
  nameCompany;
  userInfo;
  public imgUserUrlOP = '../../../assets/img/Logo-PostPay-Solid.svg';

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private companyService: CompanyService,
    private userService: UsersService,
    public dialog: MatDialog, 
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private router: Router,
  ){
  }

  async ngOnInit() {
    //this.renderer.addClass(document.body, 'dark-theme');
    this.loadInfo();
    // this.displayedColumns = ['id', 'name', 'email', 'phone', 'actions'];
  }


  loadInventory(i){
    sessionStorage.setItem('lt',i);
    this.loadRoute("inventory")
  }

  loadRoute(url){
    this.router.navigate(['/'+url])
  }

  doLogout(){
    this.userService.eraseCookie("userInfo");
    this.router.navigate(['/login']);
  }

  async loadInfo(){

    this.userInfo = await this.userService.getCookie('userInfo');
    if(this.userInfo){
      if(this.userInfo.img_url!=null)
        this.imgUserUrlOP = this.userInfo.img_url;
    }else{
      this.loadRoute('/login');
    }
  }


}