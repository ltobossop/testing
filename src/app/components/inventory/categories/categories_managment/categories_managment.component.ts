import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProvidersService } from '../../../../services/provider.service'
import {Router} from "@angular/router"
@Component({
  selector: 'app-categories-managment',
  templateUrl: './categories_managment.component.html',
  styleUrls: ['./categories_managment.component.css'],
})

export class CategoriesManagmentComponent implements OnInit {
  data;
  isNew = false;
  routeSub;
  title;
  form;

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private provService: ProvidersService,
    private router: Router,
  ){
  }

  async ngOnInit() {

    this.routeSub = this.route.params.subscribe(params => {
      this.verifyType(params['id'])
    });

  }

  async verifyType(id){
    if(id!=undefined){
      this.loadProvider(id);
    }else{
      this.newProduct();
    }
  }


  newProduct(){
    this.title = "Nueva categoria";
    this.loadForm();
  }

  async loadProvider(id){
    this.title = "Editando categoria";
    this.loadForm();
    await this.getData(id)
  }

  showSnackBar(message, action){
    this._snackBar.open(message, action, {
      duration: 2500,
    });
  }

  async getData(id){
    await this.provService.getACategory(id).subscribe(async (result:any)=>{
      console.log("hereee",result)
      await this.loadForm(result);
    },(error:any)=>{
      console.log("error", error)
    });
    return null;
  }


  loadForm(dataI?){
    this.form = this.formBuilder.group({
      id: [dataI!=null? dataI.id : null],
      key: [dataI!=null? dataI.key : "",[Validators.required]],
      name: [dataI!=null? dataI.name : "",[Validators.required]],
      description: [dataI!=null? dataI.description:"" ,[Validators.required]],
      imgUrl: [dataI!=null? dataI.imgUrl:''],
      status: [dataI!=null? dataI.status:1 ],
    });
  }


  async saveItem(){
    if(this.form.valid){
      await this.provService.saveACategory(this.form.value).subscribe((result:any)=>{
        this.router.navigate(['/inventory'])
      },(error:any)=>{
        console.log("error", error)
      });


    }
  }

}