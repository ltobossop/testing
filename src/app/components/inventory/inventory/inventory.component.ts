import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../services/user.service'
import { InventoryService } from '../../../services/inventory.service'
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort, MatTableDataSource, MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router"
import {MatPaginator} from '@angular/material/paginator';
import { MatPaginatorIntl } from '@angular/material';



export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css'],
})

export class InventoryComponent implements OnInit {
  addInventory;
  backgroundURL;
  displayedColumns: string[] = ['trafficLight','code','description','unit','currentPrice','actualStock','actions'];
  dataSource;
  nameCompany;
  title;
  data = [];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private userService: UsersService,
    private invService: InventoryService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private router: Router,
  ){
  }

  async ngOnInit() {

    await this.loadInventory();
  }

  async loadInventory(){
    await this.invService.getAllInventory().subscribe((result:any)=>{
      this.dataSource = new MatTableDataSource(result);
      this.dataSource.paginator = this.paginator;
      console.log(result)
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async ngAfterViewInit() {
      if (this.sort){
          this.dataSource.sort = this.sort;
      }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log(filterValue)
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
  
  async searchItem(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log(filterValue)
  }

  editItem(id){
    this.router.navigate(['/inventory/item/'+id])
  }

  entryItemInventory(id){
    this.router.navigate(['/inventory/item/entry/'+id])
  }
  
  outputItemInventory(id){
    this.router.navigate(['/inventory/item/output/'+id])
  }

  loadItemMovements(id){
    this.router.navigate(['/inventory/item/movements/'+id])
  }

  async deleteItem(id){
    await this.invService.deleteItemInventory(id).subscribe((result:any)=>{
      this.loadInventory();
      this.showSnackBar("Artículo eliminado con éxito","Entendido")
    },(error:any)=>{
      console.log("error", error)
      this.showSnackBar("Error al eliminar el artículo","Entendido")
    });
  }

  showSnackBar(message, action){
    this._snackBar.open(message, action, {
      duration: 2500,
    });
  }


}