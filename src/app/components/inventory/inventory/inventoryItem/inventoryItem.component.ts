import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import { InventoryService } from '../../../../services/inventory.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"
import {formatDate} from '@angular/common';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}


@Component({
  selector: 'app-inventoryItem',
  templateUrl: './inventoryItem.component.html',
  styleUrls: ['./inventoryItem.component.css'],
})

export class InventoryItemComponent implements OnInit {
  item;
  nameCompany;
  routeSub;
  title;
  public moreKeys = false;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  isNew = false;
  dataItem;
  units;
  minDate: Date;
  isPerishable = false;


  constructor(
    private userService: UsersService,
    private invService: InventoryService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router
  ){
    this.minDate = new Date();
  }

  async ngOnInit() {
    this.item = this.formBuilder.group({
      primaryCode: ['' ,  [Validators.required]],
      description: ['' ,  [Validators.required]],
      unit:['' ,  [Validators.required]],
      canSaleOutStock:['' ,  [Validators.required]],
      minStock: ['', [Validators.pattern("^[0-9]*$"),Validators.required]],
      isPersishable:['' ,  [Validators.required]],
      reorderPoint: ['', [Validators.pattern("^[0-9]*$"),Validators.required]],
      maxStock: ['', [Validators.pattern("^[0-9]*$"),Validators.required]],
    });
    this.loadUnits();
    this.routeSub = this.route.params.subscribe(params => {
      this.loadItem(params['id'])
    });
  }

  loadForm(){
    console.log(this.dataItem);
   this.item.controls["primaryCode"].setValue(this.dataItem.primaryCode);
   this.item.controls["description"].setValue(this.dataItem.description);
   this.item.controls["unit"].setValue(this.dataItem.unit.id);
   this.item.controls["canSaleOutStock"].setValue(this.dataItem.canSaleOutStock?1:0);
   this.item.controls["isPersishable"].setValue(this.dataItem.isPersishable?1:0);
   this.item.controls["reorderPoint"].setValue(this.dataItem.reorderPoint);
   this.item.controls["maxStock"].setValue(this.dataItem.maxStock);
   this.item.controls["minStock"].setValue(this.dataItem.minStock);

  }

  async loadItem(id){
    if(id!=undefined){
      this.loadExistentProduct(id);
    }else{
      this.loadCreateItem();
    }
  }

  async loadInventory(){
    await this.invService.getAllInventory().subscribe((result:any)=>{
      //this.dataSource = new MatTableDataSource(result);
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async loadUnits(){
    await this.invService.getAllUnits().subscribe((result:any)=>{
      this.units = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  showSnackBar(message, action){
    this._snackBar.open(message, action, {
      duration: 2500,
    });
  }

  async loadCreateItem(){
    this.title = "Creando un nuevo item";
    this.titleService.setTitle("Pospay - Nuevo Item")
    this.isNew = !this.isNew;
  }

  async loadExistentProduct(id){
    this.titleService.setTitle("Pospay - Editando Item")
    
    await this.invService.findItemInventory(id).subscribe((result:any)=>{
      this.dataItem = result;
      
      this.loadInfo();
    },(error:any)=>{
      console.log("error", error)
    });
  }

  loadInfo(){
    this.title = "Editando un nuevo item";
    this.titleService.setTitle("Pospay - Editando Item " + this.dataItem.key)
    this.isNew = false;
    this.loadForm();
    if(this.dataItem.expireEstimated!=null){
      this.isPerishable = !this.isPerishable;
    }
  }

  

  async showMoreKeys(e){
    e.preventDefault();
    this.moreKeys = !this.moreKeys;
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(evt: KeyboardEvent) {
    this.router.navigate(['/inventory'])
  }
  
  @HostListener('document:keydown.enter', ['$event']) onKeydownHandlerEnter(evt: KeyboardEvent) {
    this.saveItem();
  }

  async saveItem(){
    if(this.item.touched && this.item.status != "INVALID"){
      let data = this.item.value
      if(!this.isNew){
        data.id=this.item.id
      }
      data.isPersishable = data.isPersishable == 0 ? false : true
      await this.invService.saveItemInventory(data).subscribe((result:any)=>{
        this.router.navigate(['/inventory'])
        this.openSnackBar("Registro de producto exitoso.", "OK", 3000)
      },(error:any)=>{
        this.openSnackBar("Error al registrar el item.", "OK", 3000)
      });
    }else{
      this.openSnackBar("Favor de completar el formulario.", "OK", 3000)
    }
  }

  async isPersishable(){
    this.isPerishable = !this.isPerishable;
    return [true, true];
  }

  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

}