import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Component} from '@angular/core';


import { InventoryItemEntryComponent } from './inventoryItemEntry.component';


import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { Routes, RouterModule } from '@angular/router';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule, MatInputModule, MatSelectModule, MatCheckboxModule, MatProgressBarModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
@NgModule({
  declarations: [
    InventoryItemEntryComponent,
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    MatDividerModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatGridListModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    MatTabsModule,
    RouterModule,
    MatPaginatorModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    NgxMatSelectSearchModule
  ],
  exports: [InventoryItemEntryComponent],
  bootstrap: [InventoryItemEntryComponent],
})
export class InventoryItemModule { }