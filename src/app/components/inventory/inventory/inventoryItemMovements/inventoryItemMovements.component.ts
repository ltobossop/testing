import { Component, OnInit, ViewChild, Inject, HostListener, Pipe, PipeTransform } from '@angular/core';
import { InventoryService } from '../../../../services/inventory.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const UNIT_PRODUCTS =[
  {id:0, name:"Pieza", abbreviation:"PZ"},
  {id:1, name:"Gramo", abbreviation:"GR"},
  {id:2, name:"Kilo", abbreviation:"KG"},
  {id:3, name:"Onza", abbreviation:"OZ"},
  {id:4, name:"Libra", abbreviation:"LB"},
  {id:5, name:"Litro", abbreviation:"LT"},
  {id:6, name:"Mililitro", abbreviation:"ML"},
  {id:7, name:"Galón", abbreviation:"GAL"},
]

@Component({
  selector: 'app-inventoryItemMovements',
  templateUrl: './inventoryItemMovements.component.html',
  styleUrls: ['./inventoryItemMovements.component.css'],
})  


export class InventoryItemMovementsComponent implements OnInit {
  items;
  nameCompany;
  routeSub;
  title;
  public moreKeys = false;
  displayedColumns: string[] = ['type','quantity', 'unitBuy', 'price',  'provider', 'user', 'date'];
  isNew = false;
  units = UNIT_PRODUCTS;


  constructor(
    private invService: InventoryService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
  ){
  }

  async ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.loadInfo(params['id'])
    });

  }
  

  async loadInfo(id){
    await this.invService.findItemInventoryMovements(id).subscribe((result:any)=>{
      this.items = result;
      console.log(result)
      
    },(error:any)=>{
      console.log("error", error)
    });
  }


  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

  findUnitBuy(id){
    return this.units.map(o => {
      console.log(id)
      if(o.id==id){
        return o.name
      }
    });
}

}