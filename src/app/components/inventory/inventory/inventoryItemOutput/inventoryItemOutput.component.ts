import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import { InventoryService } from '../../../../services/inventory.service'
import { ProvidersService } from '../../../../services/provider.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"
import {formatDate} from '@angular/common';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const UNIT_PRODUCTS =[
  {id:0, name:"Pieza", abbreviation:"PZ"},
  {id:1, name:"Gramo", abbreviation:"GR"},
  {id:2, name:"Kilo", abbreviation:"KG"},
  {id:3, name:"Onza", abbreviation:"OZ"},
  {id:4, name:"Libra", abbreviation:"LB"},
  {id:5, name:"Litro", abbreviation:"LT"},
  {id:6, name:"Mililitro", abbreviation:"ML"},
  {id:7, name:"Galón", abbreviation:"GAL"},
] 

@Component({
  selector: 'app-inventoryItemOutput',
  templateUrl: './inventoryItemOutput.component.html',
  styleUrls: ['./inventoryItemOutput.component.css'],
})

export class InventoryItemOutputComponent implements OnInit {
  item;
  providers;
  routeSub;
  title;
  dataItem;
  units;
  minDate: Date;
  isPerishable = false;


  constructor(
    private userService: UsersService,
    private invService: InventoryService,
    private provService: ProvidersService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router
  ){
    this.minDate = new Date();
  }

  async ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.loadItem(params['id'])
    });
    this.item = this.formBuilder.group({
      quantity: ['' ,  [Validators.required]],
      reason: ['',  [Validators.required]]
    });
  }

  async loadItem(id){
    this.loadItemInventory(id);
    this.loadUnits();
  }

  async loadItemInventory(id){
    await this.invService.findItemInventory(id).subscribe((result:any)=>{
      this.dataItem = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async loadUnits(){
    await this.invService.getAllUnits().subscribe((result:any)=>{
      this.units = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }
  
  async saveItemOutput(){
    if(this.item.valid){
      let data = this.item.value;
      data[`inventory`] = this.dataItem.id;
      data[`user`] = 1;
      data[`type`] = 1;
      data['actualPrice'] = parseInt(this.dataItem.actualPrice);
      
      console.log(data)
      
      await this.invService.saveItemEntryInventory(data).subscribe((result:any)=>{
        this.openSnackBar("Salida de inventario registrada con éxito","Ok",3000)
        this.router.navigate(['/inventory'])
      },(error:any)=>{
        this.openSnackBar("Error al generar la salida, favor de validar que los datos estén correctos","Ok",3000)
        console.log("error", error)
      });

    }else{
      this.openSnackBar("Favor de llenar todos los campos", "OK", 2500)
    }
  }

  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

}