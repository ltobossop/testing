import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import { InventoryService } from '../../../../services/inventory.service'
import { ProvidersService } from '../../../../services/provider.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-inventoryItemPrice',
  templateUrl: './inventoryItemPrice.component.html',
  styleUrls: ['./inventoryItemPrice.component.css'],
})



export class InventoryItemPriceComponent implements OnInit {
  item;
  providers;
  routeSub;
  title;
  dataItem;
  units = null;
  minDate: Date;
  isPerishable = false;
  angular: any;
  


  constructor(
    private userService: UsersService,
    private invService: InventoryService,
    private provService: ProvidersService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router
  ){
    this.minDate = new Date();
  }

  async ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.loadItem(params['id'])
    });
    this.item = this.formBuilder.group({
      tax: [{value:"16", disabled: true}],
      quantity: ['' ,  [Validators.required]],
      price: ['' ,  [Validators.required]],
      unitBuy: ['' ,  [Validators.required]],
      expireSuggested: [''],
      expireEstimated: [''],
      isPersishable: [false, ],
      provider: ["", [Validators.required]],
      actualPrice: ['', [Validators.required]],
      unitSellQty: ["", [Validators.required]],
      ivaCalculatd: [{value:"", disabled: true}],
      costCalculatd: [{value:"", disabled: true}],
      profitCalculatd: [{value:"", disabled: true}],
    });

    this.item.controls['actualPrice'].value = this.dataItem.actualPrice;
  }

  async loadItem(id){
    this.loadItemInventory(id);
    this.loadProviders();
    this.loadUnits();
  }

  async loadUnits(){
    await this.invService.getAllUnits().subscribe((result:any)=>{
      this.units = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async loadItemInventory(id){
    await this.invService.findItemInventory(id).subscribe((result:any)=>{
      this.dataItem = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async loadProviders(){
    await this.provService.getAllProviders().subscribe((result:any)=>{
      this.providers = result;
      console.log("providers",result)
    },(error:any)=>{
      console.log("error", error)
    });
  }
  
  async saveItemEntry(){
    if(this.item.valid){
      let data = this.item.value;
      data[`inventory`] = this.dataItem.id;
      data[`user`] = 1;
      data[`type`] = 0;
      data['unitEquivalence'] = this.item.controls["quantity"].value;
      data['pricePerUnit'] = this.item.controls["actualPrice"].value;
      data['ivaPerUnit'] = (this.item.price / this.item.quantity) * (this.item.controls["tax"].value / 100)
      if(this.dataItem.unit.id != this.item.controls["unitBuy"].value){
        data['unitEquivalence'] = this.item.controls["quantity"].value;
        data['quantity'] = this.item.controls["unitSellQty"].value;
      }
      //data['profitPerUnit'] = document.getElementById('profitPerUnit').getAttribute('value');
      await this.invService.saveItemEntryInventory(data).subscribe((result:any)=>{
        this.openSnackBar("Entrada de inventario registrada con éxito","Ok",3000)
        this.router.navigate(['/inventory']);
      },(error:any)=>{
        this.openSnackBar("Error al generar la entrada, favor de validar que los datos estén correctos","Ok",3000)
        console.error(error)
      });

    }else{
      this.openSnackBar("Favor de llenar todos los campos", "OK", 2500)
    }
  }

  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

}