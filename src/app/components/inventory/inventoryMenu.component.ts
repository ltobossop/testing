import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../services/user.service'
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"

@Component({
  selector: 'app-inventory-menu',
  templateUrl: './inventoryMenu.component.html',
  styleUrls: ['./inventoryMenu.component.css'],
})

export class InventoryMenuComponent implements OnInit {
  displayedColumns;
  companyInfo;
  backgroundURL;
  nameCompany;
  lastTab = null;

  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private userService: UsersService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router,
  ){
  }


  setTitle(text, tab){
    this.titleService.setTitle("Pospay - "+text)

    if(tab!=null){
      sessionStorage.setItem('lt',tab);
      this.lastTab = tab;
    }else{
      this.lastTab = sessionStorage.getItem('lt');
    }

  }

  returnHome(){
    this.router.navigate(['/home'])
  }

  

  async ngOnInit() {
   this.setTitle("Productos", null)
   
    if(!await this.userService.getCookie('userInfo')){
      this.router.navigate(['/login'])
    }
  }
  


}