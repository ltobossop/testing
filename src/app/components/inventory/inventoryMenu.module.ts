import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Component} from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';

import { InventoryMenuComponent } from './inventoryMenu.component';
import { RoleModule } from './role/role.module';
import { RoleComponent } from './role/role.component';
import { InventoryComponent } from './inventory/inventory.component';
import { ProductsComponent } from './products/products.component';
import { RecipeComponent } from './products/recipe/recipe.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { CreatePromotionsComponent } from './promotions/createPromotions/createPromotions.component';
import { EditPromotionsComponent } from './promotions/editPromotions/editPromotions.component';
import { InventoryModule } from './inventory/inventory.module';
import { InventoryItemComponent } from './inventory/inventoryItem/inventoryItem.component';
import { InventoryItemEntryComponent } from './inventory/inventoryItemEntry/inventoryItemEntry.component';
 import { InventoryItemPriceComponent } from './inventory/inventoryItemPrices/inventoryItemPrice.component';
import { InventoryItemOutputComponent } from './inventory/inventoryItemOutput/inventoryItemOutput.component';
import { ProductManagmentComponent } from './products/product_managment/product_managment.component';
import { CategoriesManagmentComponent } from './categories/categories_managment/categories_managment.component';
import { ProvidersComponent } from './providers/providers.component';
import { ProviderManagmentComponent } from './providers/provider_managment/provider_managment.component';
import { InventoryItemMovementsComponent } from './inventory/inventoryItemMovements/inventoryItemMovements.component';
import { ProductCreateComponent } from './products/product_create/product_create.component';
import { ProductPriceComponent } from './products/productPrice/productPrice.component';
import { CategoriesComponent } from './categories/categories.component';


import { TitleBarModule } from '../components/title-bar/title-bar.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { Routes, RouterModule } from '@angular/router';
import { MatSelectModule } from '@angular/material';
import {MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import { StarRatingModule } from 'angular-star-rating';
import { ModifiersComponent } from './products/modifiers/modifiers.component';
import { SuggestedComponent } from './products/suggested/suggested.component';
import localeEsMX from '@angular/common/locales/es-MX';
import {MatChipsModule} from '@angular/material/chips';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
registerLocaleData(localeEsMX, 'es');
import {MatPaginatorModule} from '@angular/material/paginator';
import { NgxMaskModule, IConfig } from 'ngx-mask';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    InventoryMenuComponent,
    RoleComponent,
    InventoryComponent,
    PromotionsComponent,
    ProductsComponent,
    ProductManagmentComponent,
    CategoriesManagmentComponent,
    InventoryItemComponent,
    ProvidersComponent,
    ProviderManagmentComponent,
    InventoryItemEntryComponent,
    InventoryItemPriceComponent,
    RecipeComponent,
    ModifiersComponent,
    InventoryItemMovementsComponent,
    ProductCreateComponent,
    InventoryItemOutputComponent,
    CategoriesComponent,
    RecipeComponent,
    SuggestedComponent,
    CreatePromotionsComponent,
    EditPromotionsComponent,
    ProductPriceComponent
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    MatDividerModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatGridListModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    TitleBarModule,
    MatTabsModule,
    RouterModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    StarRatingModule.forRoot(),
    MatMenuModule,
    MatChipsModule,
    MatSlideToggleModule,
    NgxMatSelectSearchModule,
    MatPaginatorModule,
    NgxMaskModule
  ],
  exports: [InventoryMenuComponent],
  bootstrap: [InventoryMenuComponent],
  providers: [MatDatepickerModule, { provide: LOCALE_ID, useValue: 'es-MX' }]
})

export class InventoryMenuModule { }