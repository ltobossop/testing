import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import { ProductsService } from '../../../../services/product.service'
import { InventoryService } from '../../../../services/inventory.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar, throwToolbarMixedModesError} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-productPrice',
  templateUrl: './productPrice.component.html',
  styleUrls: ['./productPrice.component.css'],
})

export class ProductPriceComponent implements OnInit {
  item;
  price;
  routeSub;
  title;
  public moreKeys = false;
  isNew = false;
  dataItem;
  isPerishable = false;
  categories;
  dataSource;
  costProduct =0;


  constructor(
    private invService: InventoryService,
    private prodService: ProductsService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router
  ){
  }


  async ngOnInit() {
    this.price = this.formBuilder.group({
      price: ['' ,  [Validators.required,Validators.pattern(/^[.\d]+$/)]],
    });
    this.routeSub = this.route.params.subscribe(params => {
      this.loadItem(params['id'])
    });
  }

  async loadItem(id){
    if(id!=undefined){
      this.title = "Editando un producto"
      this.loadExistentProduct(id);
    }else{
      this.router.navigate(['/inventory'])
    }
  }

  async loadExistentProduct(id){
    this.titleService.setTitle("Pospay - Editando Item")
    await this.prodService.findProduct(id).subscribe((result:any)=>{
      this.dataItem = result;
      this.price = this.formBuilder.group({
        price: [this.dataItem.price ,  [Validators.required,Validators.pattern(/^[.\d]+$/)]],
      });
      this.getTotalCost();
    },(error:any)=>{
      console.log("error", error)
    });
  }

  getTotalCost(){
    this.dataItem.recipe.forEach((item)=>{
      this.costProduct += (item.quantity * item.inventory.actualPrice)
    });
  }


  async saveItem(){
    let data = {
      id:this.dataItem.id,
      price:this.price.controls['price'].value
    }
    console.log(data)

    await this.prodService.updateProduct(data).subscribe((result:any)=>{
      this.openSnackBar("Precio actualizado correctamente", 'OK', 3000);
      this.router.navigate(['/inventory'])
    },(error:any)=>{
      if(error.error.code == 409)
        this.openSnackBar("El producto con esa clave ya existe, intenta otra.", "OK", 3000)  
      else
        this.openSnackBar("Error al registrar el item.", "OK", 3000)
    });
  }

  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

}