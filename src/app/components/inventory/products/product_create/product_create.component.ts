import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import { ProductsService } from '../../../../services/product.service'
import { InventoryService } from '../../../../services/inventory.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar, throwToolbarMixedModesError} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"
import {formatDate} from '@angular/common';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const UNIT_PRODUCTS =[
  {id:0, name:"Pieza", abbreviation:"PZ"},
  {id:1, name:"Gramo", abbreviation:"GR"},
  {id:2, name:"Kilo", abbreviation:"KG"},
  {id:3, name:"Onza", abbreviation:"OZ"},
  {id:4, name:"Libra", abbreviation:"LB"},
  {id:5, name:"Litro", abbreviation:"LT"},
  {id:6, name:"Mililitro", abbreviation:"ML"},
  {id:7, name:"Galón", abbreviation:"GAL"},
]

@Component({
  selector: 'app-product-create',
  templateUrl: './product_create.component.html',
  styleUrls: ['./product_create.component.css'],
})

export class ProductCreateComponent implements OnInit {
  item;
  price;
  recipeItem;
  nameCompany;
  routeSub;
  title;
  public moreKeys = false;
  displayedColumns: string[] = ['code', 'description','quantity','price','actions'];
  isNew = false;
  dataItem;
  units = UNIT_PRODUCTS;
  minDate: Date;
  isPerishable = false;
  categories;
  inventory;
  dataSource;


  constructor(
    private userService: UsersService,
    private invService: InventoryService,
    private prodService: ProductsService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router
  ){
    this.minDate = new Date();
  }

  setUnit(item){
    this.recipeItem.controls["unit"].setValue(item.unit.name);
  }

  async ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.loadItem(params['id'])
    });

    this.dataSource = new MatTableDataSource();
    this.loadCategories();
    this.loadInventory();
    

    this.item = this.formBuilder.group({
      key: ['' ,  [Validators.required]],
      description: ['' ,  ],
      category: ['' ,  [Validators.required]],
      type: ['' ,  [Validators.required]],
      name: ['' ,  [Validators.required]],
    });
    
    this.price = this.formBuilder.group({
      price: ['' ,  [Validators.required,Validators.pattern(/^[.\d]+$/)]],
    });
    
    this.recipeItem = this.formBuilder.group({
      itemInventory: ['' ,  [Validators.required]],
      quantity: ['' ,  [Validators.required]],
      unit: ['' ,  [Validators.required], {disabled:true}],
    });
    this.recipeItem.controls["unit"].disable()
  }

  loadForm(){
   this.item.controls["key"].setValue(this.dataItem.key);
   this.item.controls["description"].setValue(this.dataItem.description);
   this.item.controls["name"].setValue(this.dataItem.name);
   this.item.controls["type"].setValue(this.dataItem.type);
  }

  async loadItem(id){
    if(id!=undefined){
      this.title = "Editando un producto"
      this.loadExistentProduct(id);
    }else{
      this.title = "Creando un nuevo producto"
      this.loadCreateItem();
    }
  }

  async loadInventory(){
    await this.invService.getAllInventory().subscribe((result:any)=>{
      this.inventory = result;
      console.log(result)
    },(error:any)=>{
      console.log("error", error)
    });
  }

  addToRecipe(item){
    const data = this.dataSource.data;
    let itemInventory = this.recipeItem.get('itemInventory').value;
    itemInventory.quantity = this.recipeItem.get('quantity').value;
    itemInventory.unit = item.unit;
    itemInventory.price = item.actualPrice;
    data.push(itemInventory);
    this.dataSource.data = data;
  }

  getTotalCost(){
    return this.dataSource.data.map(t => (t.quantity * t.price )).reduce((acc, value) => acc + value, 0)
  }

  deleteItem(element){
    this.dataSource.data = this.dataSource.data.filter(obj => obj.id !== element.id);
  }

  async loadCategories(){
    await this.invService.getAllCategories().subscribe((result:any)=>{
      this.categories = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async ngAfterViewInit() {
  }

  showSnackBar(message, action){
    this._snackBar.open(message, action, {
      duration: 2500,
    });
  }

  async loadCreateItem(){
    this.title = "Creando un nuevo producto";
    this.titleService.setTitle("Pospay - Nuevo Producto")
    this.isNew = !this.isNew;
  }

  async loadExistentProduct(id){
    this.titleService.setTitle("Pospay - Editando Item")
    
    await this.prodService.findProduct(id).subscribe((result:any)=>{
      this.dataItem = result;
      result.recipe.forEach((item)=>{
        this.dataSource.push(item.inventory)
      });
      this.loadInfo();
    },(error:any)=>{
      console.log("error", error)
    });
  }

  loadInfo(){
    this.title = "Editando un nuevo item";
    this.titleService.setTitle("Pospay - Editando Item " + this.dataItem.key)
    this.isNew = false;
    this.loadForm();
    if(this.dataItem.expireEstimated!=null){
      this.isPerishable = !this.isPerishable;
    }
  }

  

  async showMoreKeys(e){
    e.preventDefault();
    this.moreKeys = !this.moreKeys;
  }


  async saveItem(){
    if(this.item.touched && this.item.status != "INVALID"){
      let data = this.item.value
      data.price = this.price.controls['price'].value
      if(!this.isNew){
        data.id=this.item.id
      }
      await this.prodService.saveProduct(data).subscribe((result:any)=>{
        this.saveRecipe(result);
      },(error:any)=>{
        console.error(error)
        if(error.error.code == 409)
          this.openSnackBar("El producto con esa clave ya existe, intenta otra.", "OK", 3000)  
        else
          this.openSnackBar("Error al registrar el item.", "OK", 3000)
      });
    }else{
      this.openSnackBar("Favor de completar el formulario.", "OK", 3000)
    }
  }

  async saveRecipe(result){

    this.dataSource.data.forEach(itemRecipe => {
      let data = {
        'product':result.id,
        'unit':itemRecipe.unit.id,
        'quantity':itemRecipe.quantity,
        'inventory':itemRecipe.id,
      };
      
      this.invService.saveProductRecipe(data).subscribe((result:any)=>{
        this.openSnackBar("Producto registrado con éxito.", "OK", 3000)
        this.router.navigate(['/inventory'])
      },(error:any)=>{
        console.log(error.message)
        this.openSnackBar("Error al registrar el item.", "OK", 3000)
      });
    });


    
  }

  async isPersishable(){
    this.isPerishable = !this.isPerishable;
    return [true, true];
  }

  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

}