import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../services/user.service'
import { ProductsService } from '../../../services/product.service'
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router"

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})

export class ProductsComponent implements OnInit {
  addInventory;
  backgroundURL;
  displayedColumns: string[] = ['key','name','description','type','category','price', 'actions'];
  dataSource;
  nameCompany;

  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private userService: UsersService,
    private productsService: ProductsService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router,
    private _snackBar: MatSnackBar, 
  ){
  }

  async ngOnInit() {
    await this.loadProducts();
  }

  async loadProducts(){
    await this.productsService.getAllProducts().subscribe((result:any)=>{
      this.dataSource = new MatTableDataSource(result);
      console.log("here 1 ", result)
    },(error:any)=>{
      console.log("error", error)
    });
  }

  ngAfterViewInit() {
    
  }

  editProduct(id){
    this.router.navigate(['/product/'+id])
  }

  recipeItem(id){
    this.router.navigate(['/recipe/'+id])
  }
  
  modifiersItem(id){
    this.router.navigate(['/modifiers/'+id])
  }
  
  suggestedItem(id){
    this.router.navigate(['/suggested/'+id])
  }
  
  pricesItem(id){
    this.router.navigate(['/product/price/'+id])
  }

  async deleteItem(id){
    await this.productsService.deleteProduct(id).subscribe((result:any)=>{
      this.loadProducts();
      this.showSnackBar("Artículo eliminado con éxito","Entendido")
    },(error:any)=>{
      console.log("error", error)
      this.showSnackBar("Error al eliminar el artículo","Entendido")
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  showSnackBar(message, action){
    this._snackBar.open(message, action, {
      duration: 2500,
    });
  }


}