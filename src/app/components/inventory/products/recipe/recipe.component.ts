import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import { ProductsService } from '../../../../services/product.service'
import { InventoryService } from '../../../../services/inventory.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar, throwToolbarMixedModesError} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css'],
})

export class RecipeComponent implements OnInit {
  item;
  recipeItem;
  nameCompany;
  routeSub;
  title;
  public moreKeys = false;
  displayedColumns: string[] = ['code','description','quantity','unit','price','actions', ];
  footerColumn: string[] = ['total','price' ];
  isNew = false;
  dataItem;
  minDate: Date;
  isPerishable = false;
  categories;
  inventory;
  inventoryResp;
  dataSource;
  units;
  product=null;
  productId=null;
  isEditing = false;
  idEditing;
  tittleEditor = "Añadir item al recetario"

  constructor(
    private userService: UsersService,
    private invService: InventoryService,
    private prodService: ProductsService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router
  ){
    this.minDate = new Date();
  }

  async ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.loadItem(params['id'])
    });

    this.dataSource = new MatTableDataSource();
    this.loadInventory();
    this.loadUnits();
    this.loadForm();
  }

  getTotalCost() {
    return this.dataSource.map(t => (t.quantity * t.inventory.actualPrice)).reduce((acc, value) => acc + value, 0)
  }

  getPriceItemInventory(qty, price){
    return price * qty;
  }

  loadForm(){
    this.recipeItem = this.formBuilder.group({
      itemInventory: ['' ,  [Validators.required]],
      quantity: ['' ,  [Validators.required]],
      unit: ['' ,  [Validators.required]],
    });
    this.recipeItem.controls["unit"].disable()
  }

  async loadItem(id){
    if(id!=undefined){
      this.title = "Modificando la receta"
      this.productId = id;
      this.loadExistentProduct(id);
    }else{
      this.router.navigate(['/inventory'])
    }
  }

  async loadInventory(){
    await this.invService.getAllInventory().subscribe((result:any)=>{
      this.inventory = result;
      console.log(result)
    },(error:any)=>{
      console.log("error", error)
    });
  }
  
  async loadUnits(){
    await this.invService.getAllUnits().subscribe((result:any)=>{
      this.units = result;
      
    },(error:any)=>{
      console.log("error", error)
    });
  }

  


  async loadExistentProduct(id){
    this.titleService.setTitle("Pospay - Editando Recetario")
    
    await this.prodService.findProduct(id).subscribe((result:any)=>{
      this.dataItem = result;
      this.dataSource = result.recipe;
      this.product = result;
      this.loadInfo();
    },(error:any)=>{
      console.log("error", error)
    });
  }

  loadInfo(){
    this.title = "Editando recetario";
    this.titleService.setTitle("Pospay - Editando recetario " + this.dataItem.key)

    if(this.dataItem.expireEstimated!=null){
      this.isPerishable = !this.isPerishable;
    }
  }

  setUnit(item){
    this.recipeItem.controls["unit"].setValue(item.unit.name);
  }

  async isPersishable(){
    this.isPerishable = !this.isPerishable;
    return [true, true];
  }


  editInventory(item){
    window.scrollTo(0,document.body.scrollHeight);
    
    this.tittleEditor = "Editando item del recetario"
    this.recipeItem = this.formBuilder.group({
      itemInventory: [item.inventory ,  [Validators.required]],
      quantity: [item.quantity ,  [Validators.required]],
      unit: [item.unit.name ,  [Validators.required]],
    });
    this.recipeItem.controls["unit"].disable()
    this.recipeItem.controls["itemInventory"].disable() 

    this.inventoryResp = this.inventory;
    this.inventory = [item.inventory]
    this.isEditing = true;
    this.idEditing = item.id;
  }


  async addToRecipe(value){
    let data;

    if(this.isEditing){
      data = {
        'id':this.idEditing,
        'quantity':this.recipeItem.get('quantity').value,
        'isEdit':true
      };
    }else{
      data = {
        'product':this.productId,
        'unit':value.unit.id,
        'quantity':this.recipeItem.get('quantity').value,
        'inventory':value.id,
      };
    }

    await this.invService.saveProductRecipe(data).subscribe((result:any)=>{
      
      this.loadExistentProduct(this.productId)
      this.loadForm();
      if(this.isEditing){
        this.inventory = this.inventoryResp
        this.isEditing = false;
        this.idEditing = null;
        this.openSnackBar("Item modificado con éxito.", "OK", 3000)
      }else{
        this.openSnackBar("Item añadido al recetario con éxito.", "OK", 3000)
      }
      this.tittleEditor = "Añadir item al recetario"
    },(error:any)=>{
      console.log(error.message)
      this.openSnackBar("Error al modificar el item.", "OK", 3000)
    });
  }

  async deleteItem(item){

    await this.invService.deleteItemRecipe(item).subscribe((result:any)=>{
      this.loadExistentProduct(this.productId)
      this.openSnackBar("Item eliminado de este recetario con éxito.", "OK", 3000)
      this.loadExistentProduct(this.productId)
    },(error:any)=>{
      console.log(error.message)
      this.openSnackBar("Error al eliminar el item.", "OK", 3000)
    });
  }

  cancelEditing(){
    this.tittleEditor = "Añadir item al recetario"
    this.inventory = this.inventoryResp
    this.isEditing = false;
    this.idEditing = null;
    this.loadForm();
    window.scrollTo(0,0);
  }

  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

}