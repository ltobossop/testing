import { Component, OnInit, ViewChild, Inject, HostListener } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import { ProductsService } from '../../../../services/product.service'
import { InventoryService } from '../../../../services/inventory.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar, throwToolbarMixedModesError} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router"
import {formatDate} from '@angular/common';


@Component({
  selector: 'app-suggested',
  templateUrl: './suggested.component.html',
  styleUrls: ['./suggested.component.css'],
})

export class SuggestedComponent implements OnInit {
  item;
  recipeItem;
  nameCompany;
  routeSub;
  title;
  public moreKeys = false;
  displayedColumns: string[] = ['code','description','quantity','unit','actions'];
  isNew = false;
  dataItem;
  minDate: Date;
  isPerishable = false;
  categories;
  inventory;
  inventoryResp;
  dataSource;
  units;
  product=null;
  productId=null;
  isEditing = false;
  idEditing;
  tittleEditor = "Añadir sugerencia de items"

  constructor(
    private userService: UsersService,
    private invService: InventoryService,
    private prodService: ProductsService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router
  ){
    this.minDate = new Date();
  }

  async ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.loadItem(params['id'])
    });

    this.dataSource = new MatTableDataSource();
    this.loadInventory();
    this.loadUnits();
    this.loadForm();
  }

  loadForm(){
    this.recipeItem = this.formBuilder.group({
      itemInventory: ['' ,  [Validators.required]],
      quantity: ['' ,  [Validators.required]],
      unit: ['' ,  [Validators.required]],
    });
    this.recipeItem.controls["unit"].disable()
  }

  async loadItem(id){
    if(id!=undefined){
      this.title = "Modificando el modificador"
      this.productId = id;
      this.loadExistentProduct(id);
    }else{
      this.router.navigate(['/inventory'])
    }
  }

  async loadInventory(){
    await this.invService.getAllInventory().subscribe((result:any)=>{
      this.inventory = result;
      console.log(result)
    },(error:any)=>{
      console.log("error", error)
    });
  }
  
  async loadUnits(){
    await this.invService.getAllUnits().subscribe((result:any)=>{
      this.units = result;
      
    },(error:any)=>{
      console.log("error", error)
    });
  }

  


  async loadExistentProduct(id){
    this.titleService.setTitle("Pospay - Editando sugerencias")
    
    await this.prodService.findProductSuggested(id).subscribe((result:any)=>{
      this.dataItem = result;
      this.dataSource = result.suggested;
      this.product = result;
      console.log(result)
      this.loadInfo();
    },(error:any)=>{
      console.log("error", error)
    });
  }

  loadInfo(){
    this.title = "Editando sugerencias";
    this.titleService.setTitle("Pospay - Editando sugerencias " + this.dataItem.key)

    if(this.dataItem.expireEstimated!=null){
      this.isPerishable = !this.isPerishable;
    }
  }

  setUnit(item){
    this.recipeItem.controls["unit"].setValue(item.unit.name);
  }

  async isPersishable(){
    this.isPerishable = !this.isPerishable;
    return [true, true];
  }


  editInventory(item){
    window.scrollTo(0,document.body.scrollHeight);
    
    this.tittleEditor = "Editando item del recetario"
    this.recipeItem = this.formBuilder.group({
      itemInventory: [item.inventory ,  [Validators.required]],
      quantity: [item.quantity ,  [Validators.required]],
      unit: [item.unit.name ,  [Validators.required]],
    });
    this.recipeItem.controls["unit"].disable()
    this.recipeItem.controls["itemInventory"].disable() 

    this.inventoryResp = this.inventory;
    this.inventory = [item.inventory]
    this.isEditing = true;
    this.idEditing = item.id;
  }


  async addToRecipe(value){
    let data;

    if(this.isEditing){
      data = {
        'id':this.idEditing,
        'quantity':this.recipeItem.get('quantity').value,
        'isEdit':true
      };
    }else{
      data = {
        'product':this.productId,
        'unit':value.unit.id,
        'quantity':this.recipeItem.get('quantity').value,
        'inventory':value.id,
      };
    }

    await this.invService.saveProductSuggested(data).subscribe((result:any)=>{
      
      this.loadExistentProduct(this.productId)
      this.loadForm();
      if(this.isEditing){
        this.inventory = this.inventoryResp
        this.isEditing = false;
        this.idEditing = null;
        this.openSnackBar("Item modificado con éxito.", "OK", 3000)
      }else{
        this.openSnackBar("Sugerencia añadida con éxito.", "OK", 3000)
      }
      this.tittleEditor = "Añadir sugerencia"
    },(error:any)=>{
      console.log(error.message)
      this.openSnackBar("Error al modificar el item.", "OK", 3000)
    });
  }

  async deleteItem(item){

    await this.invService.deleteItemSuggested(item).subscribe((result:any)=>{
      this.loadExistentProduct(this.productId)
      this.openSnackBar("Sugerencia con éxito.", "OK", 3000)
    },(error:any)=>{
      console.log(error.message)
      this.openSnackBar("Error al eliminar la sugerencia.", "OK", 3000)
    });
  }

  cancelEditing(){
    this.tittleEditor = "Añadir sugerencia"
    this.inventory = this.inventoryResp
    this.isEditing = false;
    this.idEditing = null;
    this.loadForm();
    window.scrollTo(0,0);
  }

  openSnackBar(message: string, action: string, time:number) {
    this._snackBar.open(message, action, {
      duration: time,
    });
  }

}