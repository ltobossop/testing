import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProvidersService } from '../../../../services/provider.service'
import { ProductsService } from '../../../../services/product.service'
import { InventoryService } from '../../../../services/inventory.service'
import {Router} from "@angular/router"
import { ifStmt } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-edit-promotions',
  templateUrl: './editPromotions.component.html',
  styleUrls: ['./editPromotions.component.css'],
})

export class EditPromotionsComponent implements OnInit {
  data;
  isNew = false;
  routeSub;
  title;
  form;
  recipeItem;
  products;
  inventory;
  dataSource;
  type = 0;
  displayedColumns: string[] = ['code', 'name','category','quantity','actions'];
  actualProductsArray = [];
  unitDinamic = "";

  hours = [
    "00:00",
    "01:00",
    "02:00",
    "03:00",
    "04:00",
    "05:00",
    "06:00",
    "07:00",
    "08:00",
    "09:00",
    "10:00",
    "11:00",
    "12:00",
    "13:00",
    "14:00",
    "15:00",
    "16:00",
    "17:00",
    "18:00",
    "19:00",
    "20:00",
    "21:00",
    "22:00",
    "23:00",
  ]

  formsLabels={
    formName:"Cargando",
    formDescription:"Cargando",
    formItemTitle:"Cargando",
    formItemName:"Cargando",
    formItemError:"Cargando",
  }
  myControl;
  promotionInfo;
  promotionId;


  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private provService: ProvidersService,
    private router: Router,
    private productsService: ProductsService,
    private inventoryService: InventoryService,
  ){
    this.myControl = new FormControl();

  }

  setDinamicUnit(itm){
    this.unitDinamic = this.type == 0 ? "" : itm.unit.name+'s' ;
  }

  addToRecipe(item){
    const data = this.dataSource.data;
    if(this.type==0){
      let itemInventory = this.recipeItem.get('itemInventory').value;
      itemInventory.quantity = this.recipeItem.get('quantity').value;
      itemInventory.unit = 'Piezas';
      itemInventory.category = item.category.name;
      data.push(itemInventory);
    }else{
      
      let dataT = {
        key : item.primaryCode,
        name : item.description,
        quantity : this.recipeItem.get('quantity').value,
        unit : item.unit.name,
        category : "Inventario",
      };
      data.push(dataT);
    }

    this.dataSource.data = data;
  }

  async ngOnInit() {

    this.loadProduct();
    this.loadFormProduct();
    this.dataSource = new MatTableDataSource();
    await this.setType(0)
  }

  loadFormProduct(){
    this.recipeItem = this.formBuilder.group({
      itemInventory: ['' ,  [Validators.required]],
      quantity: ['' ,  [Validators.required]],
    });
  }


  async loadProducts(){
    await this.productsService.getAllProducts().subscribe((result:any)=>{
      this.products = result;
      this.actualProductsArray = [];
      this.actualProductsArray = this.products;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async changeToDiscount(){
    await this.inventoryService.getAllInventory().subscribe((result:any)=>{
      this.inventory = result;
      this.actualProductsArray = [];
      this.actualProductsArray = this.inventory;
      console.log("inventory=>>",this.inventory)
    },(error:any)=>{
      console.log("error", error)
    });
  }
  
  async loadInventory(){
    await this.productsService.getAllProducts().subscribe((result:any)=>{
      this.products = result;
      this.actualProductsArray = this.products;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async setType(type){
    this.type = type

    if(type==0){
      this.formsLabels.formName = "Nombre del combo";
      this.formsLabels.formDescription = "Descripción del combo";
      this.formsLabels.formItemTitle = "Agregar nuevo producto";
      this.formsLabels.formItemName = "Producto";
      this.formsLabels.formItemError = "Selecciona un producto.";
      await this.loadProducts();
    }else{
      this.formsLabels.formName = "Nombre del descuento";
      this.formsLabels.formDescription = "Descripción del descuento";
      this.formsLabels.formItemTitle = "Agregar nuevo item del inventario";
      this.formsLabels.formItemName = "Inventario";
      this.formsLabels.formItemError = "Selecciona un item del inventario.";
      await this.changeToDiscount();
    }
  }

  loadProduct(){
    this.title = "Editando una promoción";
    this.loadForm();
  }

  showSnackBar(message, action){
    this._snackBar.open(message, action, {
      duration: 2500,
    });
  }

  async getData(id){
    await this.provService.getACategory(id).subscribe(async (result:any)=>{
      await this.loadForm();
    },(error:any)=>{
      console.log("error", error)
    });
    return null;
  }


  async loadForm(){
    this.routeSub = this.route.params.subscribe(params => {
      this.promotionId = params['id'];
    });

    this.form = this.formBuilder.group({
      name: ["",[Validators.required]],
      description: ["",[Validators.required]],
      vigencyStart: ["",[Validators.required]],
      hourStart: ['',[Validators.required]],
      vigencyEnd: ['',[Validators.required]],
      hourEnd: ['',[Validators.required]],
      employeDiscount: ['',[Validators.required]],
      maximunBuy: ['',[Validators.required]],
      monday: [''],
      tuesday: [''],
      wednesday: [''],
      thursday: [''],
      friday: [''],
      saturday: [''],
      sunday: [''],
      allDays: [''],
    });

    await this.provService.getAPromotion(this.promotionId).subscribe(async (result:any)=>{
      console.log("here=>",result, "this.promotionId", this.promotionId);
      if(result==undefined)
        this.router.navigate(['/inventory']);
      
      this.promotionInfo = result;
      await this.setType(this.promotionInfo.type)
      this.form = this.formBuilder.group({
        name: [this.promotionInfo.name,[Validators.required]],
        description: [this.promotionInfo.description,[Validators.required]],
        vigencyStart: [this.promotionInfo.vigencyStart,[Validators.required]],
        hourStart: [this.promotionInfo.vigencyStart,[Validators.required]],
        vigencyEnd: [this.promotionInfo.vigencyEnd,[Validators.required]],
        hourEnd: [this.promotionInfo.vigencyEnd,[Validators.required]],
        employeDiscount: [this.promotionInfo.employeDiscount,[Validators.required]],
        maximunBuy: [this.promotionInfo.maximunBuy,[Validators.required]],
        monday: [''],
        tuesday: [''],
        wednesday: [''],
        thursday: [''],
        friday: [''],
        saturday: [''],
        sunday: [''],
        allDays: [''],
      });
    });
  }

  async saveItem(){

    let days;

    if(this.form.controls['allDays'].value){
      days = '[8]'
    }else{

      days = '[';

      if(this.form.controls['monday'].value)
        days+='1,'
      if(this.form.controls['tuesday'].value)
        days+='2,'
      if(this.form.controls['wednesday'].value)
        days+='3,'
      if(this.form.controls['thursday'].value)
        days+='4,'
      if(this.form.controls['friday'].value)
        days+='5,'
      if(this.form.controls['saturday'].value)
        days+='6,'
      if(this.form.controls['sunday'].value)
        days+='7'
      else
        days.slice(0, -1)

      let l = days.split('').length;
      let ar = days.split('');
      if(ar[l] == ','){
        days = days.slice(0,-1)
      }
      days += ']';
    }


    let data = {
      "type":this.type,
      "name":this.form.controls['name'].value,
      "description":this.form.controls['description'].value,
      "vigencyStart":this.trasnformTimeStap(this.form.controls['vigencyStart'].value,this.form.controls['hourStart'].value),
      "vigencyEnd":this.trasnformTimeStap(this.form.controls['vigencyEnd'].value,this.form.controls['hourEnd'].value),
      "employeDiscount":this.form.controls['employeDiscount'].value,
      "maximunBuy":this.form.controls['maximunBuy'].value,
      "daysActive":days,
    }

    console.log("dataaa=> ", data)


    if(this.form.valid){
      await this.provService.saveAPromotion(data).subscribe((result:any)=>{
        this.router.navigate(['/inventory'])
      },(error:any)=>{
        console.log("error", error)
      });


    }
  }


  trasnformTimeStap(dateI, hour){
    let date = new Date(dateI);

    
    let hourr = parseInt(hour.slice(0,2))
    console.log(hourr)
    date.setHours(hourr)
    date.setMinutes(0)
    date.setSeconds(0)
    date.setMilliseconds(0)
    return date;
  }

}