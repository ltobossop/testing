import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ProvidersService } from '../../../services/provider.service'
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router"

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.css'],
})

export class PromotionsComponent implements OnInit {
  addInventory;
  backgroundURL;
  displayedColumns: string[] = ['name','price','startAt','endAt','days','type','actions'];
  dataSource;
  nameCompany;
  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private ps: ProvidersService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router,
  ){
  }

  async ngOnInit() {
    this.loadCategories();
  }

  async loadCategories(){
    await this.ps.getAllPromotions().subscribe((result:any)=>{
      console.log(result)
      this.data = result;
      this.dataSource = new MatTableDataSource(this.data);
      //this.dataSource.paginator = this.paginator;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async deleteProvider(id){
    await this.ps.deleteProvider(id).subscribe((result:any)=>{
      this.loadCategories();
      console.log(result)
    },(error:any)=>{
      console.log("error", error)
    });
  }

  editItem(id){
    this.router.navigate(['/category/'+id])
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    if (this.sort){
      this.dataSource.sort = this.sort;
    }
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}