import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID,NgModule } from '@angular/core';
import {Component} from '@angular/core';
import { registerLocaleData } from '@angular/common';


import { PromotionsComponent } from './promotions.component';



import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { Routes, RouterModule } from '@angular/router';
import {MatPaginatorModule} from '@angular/material/paginator';
import localeEsMX from '@angular/common/locales/es-MX';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

registerLocaleData(localeEsMX, 'es');


@NgModule({
  declarations: [
    PromotionsComponent,
    
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    MatDividerModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatGridListModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    MatTabsModule,
    RouterModule,
    MatPaginatorModule,
    MatAutocompleteModule
  ],
  exports: [PromotionsComponent],
  bootstrap: [PromotionsComponent],
  providers: [ { provide: LOCALE_ID, useValue: 'es-MX' } ],
})
export class PromotionsModule { }