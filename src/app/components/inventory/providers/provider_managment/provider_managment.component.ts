import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../../services/user.service'
import {MatSort, MatTableDataSource, MatPaginator, MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProvidersService } from '../../../../services/provider.service'
import {Router} from "@angular/router"
@Component({
  selector: 'app-provider-managment',
  templateUrl: './provider_managment.component.html',
  styleUrls: ['./provider_managment.component.css'],
})

export class ProviderManagmentComponent implements OnInit {
  data;
  isNew = false;
  routeSub;
  title;
  form;

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private provService: ProvidersService,
    private router: Router,
  ){
  }

  async ngOnInit() {

    this.routeSub = this.route.params.subscribe(params => {
      this.verifyType(params['id'])
    });

  }

  async verifyType(id){
    if(id!=undefined){
      this.loadProvider(id);
    }else{
      this.newProduct();
    }
  }


  newProduct(){
    this.title = "Nuevo proveedor";
    this.loadForm();
  }

  async loadProvider(id){
    this.title = "Editando proveedor";
    this.loadForm();
    await this.getData(id)
  }

  showSnackBar(message, action){
    this._snackBar.open(message, action, {
      duration: 2500,
    });
  }

  async getData(id){
    await this.provService.getAProvider(id).subscribe(async (result:any)=>{
      console.log("hereee",result)
      await this.loadForm(result);
    },(error:any)=>{
      console.log("error", error)
    });
    return null;
  }


  loadForm(dataI?){
    console.log("dataI",dataI)
    this.form = this.formBuilder.group({
      id: [dataI!=null? dataI.id : null],
      name: [dataI!=null? dataI.name : "",[Validators.required]],
      description: [dataI!=null? dataI.description:"" ,[Validators.required]],
      phone: [dataI!=null? dataI.phone:"" ,  [Validators.pattern("^[0-9]*$"),Validators.required]],
      direction: [dataI!=null? dataI.direction:"" ,[Validators.required]],
      speed: [dataI!=null? dataI.speed:3],
      qualityProduct: [dataI!=null? dataI.qualityProduct:3 ],
      qualityService: [dataI!=null? dataI.qualityService:3 ],
      expensive: [dataI!=null? dataI.expensive:3 ],
      reliability: [dataI!=null? dataI.reliability:3 ],
      status: [dataI!=null? dataI.status:1 ],
    });
  }


  async saveItem(){
    console.log(this.form)

    if(this.form.valid){

      await this.provService.saveAProvider(this.form.value).subscribe((result:any)=>{
        //this.dataSource = new MatTableDataSource(result);
        this.router.navigate(['/inventory'])
      },(error:any)=>{
        console.log("error", error)
      });


    }
  }

}