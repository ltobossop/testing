import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ProvidersService } from '../../../services/provider.service'
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router"

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}



@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.css'],
})

export class ProvidersComponent implements OnInit {
  addInventory;
  backgroundURL;
  displayedColumns: string[] = ['id','name','description','phone', 'direction', 'actions'];
  dataSource;
  nameCompany;
  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private providerService: ProvidersService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router,
  ){
  }

  async ngOnInit() {
    this.loadProviders();
  }

  async loadProviders(){
    await this.providerService.getAllProviders().subscribe((result:any)=>{
      this.data = result;
      this.dataSource = new MatTableDataSource(this.data);
      //this.dataSource.paginator = this.paginator;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  async deleteProvider(id){
    await this.providerService.deleteProvider(id).subscribe((result:any)=>{
      this.loadProviders();
      console.log(result)
    },(error:any)=>{
      console.log("error", error)
    });
  }

  editItem(id){
    this.router.navigate(['/provider/'+id])
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    if (this.sort){
      this.dataSource.sort = this.sort;
    }
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}