import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../services/user.service'
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css'],
})

export class RoleComponent implements OnInit {
  displayedColumns;
  companyInfo;
  backgroundURL;
  nameCompany;

  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private userService: UsersService,
    public dialog: MatDialog
  ){
  }

  async ngOnInit() {
    await this.loadRoles();
  }

  ngAfterViewInit() {
  }

  async loadRoles(){

    console.log("Hello")
    await this.userService.getAllUsersRoles().subscribe((result:any)=>{
      this.data = result;
      console.log("here", this.data)
    },(error:any)=>{
      console.log("error", error)
    });
  }

}