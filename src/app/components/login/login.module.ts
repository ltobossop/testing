import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule, MatInputModule, MatSelectModule, MatCheckboxModule, MatProgressBarModule} from '@angular/material';
import { LoginComponent } from './login.page';

const routes: Routes = [
  {
    path: '',
    // component: LoginComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    MatProgressBarModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule  
  ],
  providers: [
  ],
  declarations: [LoginComponent]
})
export class LoginPageModule {}
