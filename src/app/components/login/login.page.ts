import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';

import { interval } from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UsersService } from '../../services/user.service'

const interUpdateVersion = interval(300000);

@Component({
  selector: 'suite-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginComponent implements OnInit {

  isDisabled = false
  item;

  constructor(
    private userService: UsersService,
    private router: Router,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) { }

  async ngOnInit() {
    this.item = this.formBuilder.group({
      username: ['' ,  [Validators.required]],
      password: ['' ,  [Validators.required]]
    });
  }

  async doLogin(){
    console.log(this.item)
    if(this.item.valid){
      this.showLoading();
      this.loadInfo();
    }
    
  }

  async loadInfo(){
    const user = {
      username: this.item.value.username,
      password: this.item.value.password,
      grant_type: 'password',
    };

    this.userService.post_login(user).subscribe((result:any)=>{
        delete result.data.user.password;
        delete result.data.user.salt;
        delete result.data.user.isDeleted;
        delete result.data.user.operation_code;
        delete result.data.user.codeSalt;
        this.userService.setCookie("userInfo", JSON.stringify(result.data.user), 2);
        this.router.navigate(["/home"])
    },(error:any)=>{
      this.isDisabled = false;
      this.openSnackBar("Error, los datos son inválidos","OK")
    });

  }

  forgetPassword(){
    this.router.navigate(['/passwordRecovery'])
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  

  

  async showLoading() {
    this.isDisabled = true;
  }

}
