import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule, MatInputModule, MatSelectModule, MatCheckboxModule, MatProgressBarModule} from '@angular/material';
import { PasswordRecoveryComponent } from './passwordRecovery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
  ],
  declarations: [PasswordRecoveryComponent]
})
export class LoginPageModule {}
