import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';

import { interval } from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UsersService } from '../../services/user.service'

@Component({
  selector: 'app-passwordRecovery',
  templateUrl: './passwordRecovery.page.html',
  styleUrls: ['./passwordRecovery.page.css']
})
export class PasswordRecoveryComponent implements OnInit {

  isDisabled = false
  item;

  constructor(
    private userService: UsersService,
    private router: Router,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) { }

  async ngOnInit() {
    this.item = this.formBuilder.group({
      email: ['' ,  [Validators.required, Validators.email]],
    });
  }
  
  async sendEmailRecovery(){
    if(this.item.valid){
      this.showLoading();
      this.loadInfo();
    }
    
  }

  async loadInfo(){

    const data = {
      "email": this.item.value.email
    }
    
    this.userService.sendEmailRecoveryPassword(data).subscribe((result:any)=>{
      this.router.navigate(['/login'])
      this.openSnackBar("Se envió correctamente el correo, favor de verificar en tu bandeja de entrada","OK")
    },(error:any)=>{
      this.isDisabled = false;
      this.openSnackBar("Error, los datos son inválidos","OK")
    });

  }

  goLogin(){
    this.router.navigate(['/login'])
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }

  async showLoading() {
    this.isDisabled = true;
  }

}
