import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { TableService } from '../../services/table.service'
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {
  CdkDrag,
  CdkDragStart,
  CdkDropList, CdkDropListContainer, CdkDropListGroup,
  moveItemInArray,
  CdkDragDrop,
  transferArrayItem
} from "@angular/cdk/drag-drop";
import { DndDropEvent, DropEffect } from "ngx-drag-drop";
import { MatSnackBar } from "@angular/material/snack-bar";
import {Title} from "@angular/platform-browser";

interface NestableListItem {
  content:string;
  disable?:boolean;
  handle?:boolean;
  customDragImage?:boolean;
  children?:NestableListItem[];
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})


export class TableComponent implements OnInit {
  displayedColumns;
  companyInfo;
  backgroundURL;
  nameCompany;
  zones = []
  typeTables;
  tables;
  tablesList:any[] = [];
  draggableList = [];
  zoneName = "Sin zonas registradas";
  activeZoneId;

  layout:any;
  horizontalLayoutActive:boolean = false;
  
  private readonly verticalLayout = {
    container: "row",
    list: "column",
    dndHorizontal: false
  };
  private readonly horizontalLayout = {
    container: "row",
    list: "row",
    dndHorizontal: true
  };


  public target: CdkDropList;
  public targetIndex: number;
  public source: CdkDropListContainer;
  public sourceIndex: number;
  

  data = [];

  constructor(
    private tableService: TableService,
    public dialog: MatDialog,
    private titleService: Title,
  ){ this.target = null;
    this.source = null;
    this.setHorizontalLayout( this.horizontalLayoutActive );
    this.titleService.setTitle("Pospay - Mesas")
  }

  async ngOnInit() {
    await this.loadInfo();
  }


  async loadInfo(){
    await this.tableService.getAllInfo().subscribe(async (result:any)=>{
      console.log(result);
      this.data = result;
      this.zones = result.zones;
      this.typeTables = result.typeTables;
      this.tables = result.tables;

      await this.loadTypeTables();
      await this.loadTablesPreloaded();
      this.loadFirstZone();
    },(error:any)=>{
      console.log("error", error);
    });
  }

  loadFirstZone(){
    if(this.zones.length>0){
      this.zoneName = this.zones[0].name;
      this.activeZoneId = this.zones[0].id;
      this.loadZone(this.zones[0].id);
    }
  }

  loadZone(id){
    this.draggableList = [];
    this.tables.map((f:any)=>{
      if(f.zones && f.zones.id == id){
        this.draggableList.push(f);
      }
    })
    this.zones.find((f:any, )=>{
      if(f.id == id){
        console.log("i found it")
        this.zoneName = f.name
        this.activeZoneId = f.id;
        return true;
      }
    });
  }

  deleteTable(item){
    console.log(item)
  }


  async loadTypeTables(){
    await this.typeTables.forEach(element => {
      let tbl = {
        id:element.id,
        content: element.name,
        width:element.width,
        height:element.height,
        effectAllowed: "copy",
        disable: false,
        handle: false,
      }
      this.tablesList.push(tbl);
      console.log(this.tablesList)
    });
  }

  async loadTablesPreloaded(){
    await this.tables.map(n => {
      let item = n;
      item.effectAllowed="copy";
      item.disable = false;
      item.handle = false;
      this.draggableList.push(item)
    });
  }

  getRandomColor(){
    let a = '#'+Math.floor(Math.random()*16777215).toString(16);
    console.log("hello", a)
    return a;
  }

  onDragged( item:any, list:any[], effect:DropEffect ) {
    if( effect === "copy" ) {
      console.log(item)
      const index = list.indexOf( item );
      //list.splice( index, 1 );
    }
    if( effect === "move" ) {
      console.log(item)
      const index = list.indexOf( item );
      list.splice( index, 1 );
    }
  }

  onDrop( event:DndDropEvent, list?:any[], isListTables:boolean=false) {
    console.log("here")
    if( list
      && (event.dropEffect === "copy"
        || event.dropEffect === "move") && !isListTables) {
      let index = event.index;
      if( typeof index === "undefined" ) {
        index = list.length;
      }
      list.splice( index, 0, event.data );
    }
  }

  setHorizontalLayout( horizontalLayoutActive:boolean ) {
    this.layout = (horizontalLayoutActive) ? true : true;
  }

  async saveZoneTable(){
    let zone;
    this.zones.find((f:any, )=>{
      if(f.id == this.activeZoneId){
        zone = f;
        return true;
      }
    });
    await this.draggableList.forEach(async (item:any)=>{
      let data:any = {
        isMain:true,
        zone:zone.id,
        status:'open',
        x:1,
        y:1
      };
      if(item.typeTable){
        data.typeTable = item.typeTable.id;
        data.type = item.typeTable.id;
        data.name = item.typeTable.name;
      }else{
        data.typeTable = item.id;
        data.type = item.id;
        data.name = item.content;
      }
      await this.tableService.saveAllTables(data).subscribe(async (result:any)=>{
      },async (error:any)=>{
        await this.tableService.saveAllTables(data).subscribe(async (result:any)=>{
          console.log(result)
        }); 
      });


    });

  }

}
