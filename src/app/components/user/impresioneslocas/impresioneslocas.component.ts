import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../services/user.service'
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import { PrintService, UsbDriver, WebPrintDriver } from 'ng-thermal-print';
import { PrintDriver } from 'ng-thermal-print/lib/drivers/PrintDriver';

@Component({
  selector: 'app-print',
  templateUrl: './impresioneslocas.component.html',
  styleUrls: ['./impresioneslocas.component.css'],
})

export class ImpresionesLocasComponent implements OnInit {

  status: boolean = false;
  usbPrintDriver: UsbDriver;
  webPrintDriver: WebPrintDriver;
  ip: string = '';

  constructor(
    private userService: UsersService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private printService:PrintService
  ){
    this.usbPrintDriver = new UsbDriver();
    this.printService.isConnected.subscribe(result => {
      this.status = result;
      if (result) {
          console.log('Connected to printer!!!');
          console.log(this.status)
      } else {
      console.log('Not connected to printer.');
      }
  });
  }

  requestUsb() {
    this.usbPrintDriver.requestUsb().subscribe(result => {
        this.printService.setDriver(this.usbPrintDriver, 'ESC/POS');
    });
}

connectToWebPrint() {
    this.webPrintDriver = new WebPrintDriver(this.ip);
    this.printService.setDriver(this.webPrintDriver, 'WebPRNT');
}

print(driver: PrintDriver) {
    this.printService.init()
        .setBold(true)
        .setJustification('center')
        /*.writeLine('Pospay \n')
        .setBold(false)
        .writeLine("El mejor sistema para tu negocio")
        .setUnderline(true)
        .writeLine("Pedro Simon Laplace")
        .writeLine("#5858 Col. Jardines del Sol")
        .setUnderline(false)
        .setJustification('left')
        .writeLine("Folio: 3456754321456")
        .setBold(true)
        .setJustification('center')
        .writeLine('')
        .writeLine('Cuenta 3312')
        .writeLine('')
        .setJustification('left')
        .writeLine('Usuario: Pablo \t  \t \t \t \t  \t 17')*/
        .setSize('large')
        .setInverse(true)
        .feed(3)
        .cut('partial')
        .flush();
}

  async ngOnInit() {
    
  }

  

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}