import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import {UsersService} from '../../../../services/user.service';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'dialog-role',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class EditRoleComponent implements OnInit{

  @ViewChild('colorP', null) colorPicked:ElementRef;

  title = "Nuevo rol"
  userForm;
  colorSelected;
  isEdit = false;
  color;

  constructor(
    private userService: UsersService,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EditRoleComponent>,
    private _snackBar: MatSnackBar,
  ){
    this.loadForm();
    if(this.data!=null)
      this.editMode();
    else
      this.createMode();
  }

  async ngOnInit() {
    
  }

  

  editMode(){
    this.title = "Editando rol";
    this.isEdit = true;
    this.color = this.data.color;
    this.userForm.controls['colorP'].setValue(this.data.color)
  }

  createMode(){
    this.title = "Creando nuevo rol";
  }

  loadForm(){
    this.userForm = this.formBuilder.group({
      name: this.data!=null ? this.data.name : '',
      colorP: this.data!=null ? this.data.colorP : '',
      //maxMoney: this.data!=null ? this.data.maxMoney : '',
    });
  }

  closeDialog(){
    this.dialogRef.close();
  }

  async saveRole(){

    let data = this.userForm.value;

    data.color = data.colorP!="" ? this.colorPicked.nativeElement.value : "#" + Math.floor(Math.random()*16777215).toString(16);
    delete data.colorP;
    data.isDeleted = false;
    data.canDeleted = true;
    data.canEdited = true;
    if(this.isEdit){
      data.id = this.data.id;
      this.userService.editRole(data).subscribe((result:any)=>{
        this.closeDialog();
        this.openSnackBar("Se ha editado el rol con éxito", "Aceptar");
      });
    }else{
      this.userService.createRole(data).subscribe((result:any)=>{
        this.closeDialog();
        this.openSnackBar("Se ha creado el rol con éxito", "Aceptar");
      });
    }
    console.log(data)

  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  
}
