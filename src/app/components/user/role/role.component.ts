import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../services/user.service'
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DeleteDialogComponent } from '../../components/delete-dialog/delete.component';
import { EditRoleComponent } from './edit/dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css'],
})

export class RoleComponent implements OnInit {
  displayedColumns;
  companyInfo;
  backgroundURL;
  nameCompany;
  img_url = "https://ui-avatars.com/api/?name="

  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private userService: UsersService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ){
  }

  async ngOnInit() {
    await this.loadRoles();
  }

  deleteDialog(id){
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        "title":"¿Estás seguro que deseas eliminar este rol?",
        "buttonText":"Eliminar rol",
        "contentText":"Esta acción es irreversible, además, los usuarios asignados a este rol quedarán sin un rol asignado"
      }
    });
    dialogRef.afterClosed().subscribe(async (result) => {
      if(result){
        await this.userService.deleteRole(id).subscribe((result:any)=>{
          this.loadRoles();
          this._snackBar.open("El rol ha sido eliminado con éxito", "OK", {
            duration: 3000,
          });
        },(error:any)=>{
        });
      }
    });
  }
  
  editDialog(rol){
    const dialogRef = this.dialog.open(EditRoleComponent, {
      data: rol
    });

    dialogRef.afterClosed().subscribe(()=>{
      this.loadRoles();
    });
    
  }
  
  createDialog(){
    const dialogRef = this.dialog.open(EditRoleComponent, {
    });
    dialogRef.afterClosed().subscribe(()=>{
      this.loadRoles();
    });
  }


  getAllNamesLeftUsers(users){
    let u = users.slice(3, users.length)
    let names = "";
    u.forEach(element => {
      if(names=="")
        names =  element.name + ' ' + element.lastname;
      else
        names = names + ", " + element.name + ' ' + element.lastname;
    })
    return names;
  }
  
  ngAfterViewInit() {
  }

  async loadRoles(){
    await this.userService.getAllUsersRoles().subscribe((result:any)=>{
      this.data = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }

}