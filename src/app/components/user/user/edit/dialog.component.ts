import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import {UsersService} from '../../../../services/user.service';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'dialog-create-employee',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit{

  userForm;
  isCreating = true;
  roles = null;
  img_url = 'https://ui-avatars.com/api/?name=Pos+Pay&background=random';
  @ViewChild('img_urlInput', null) myDiv: ElementRef<HTMLElement>;
  selectedFileF = null;
  titlekey = "";
  isEdit = false;


  constructor(
    private userService: UsersService,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<DialogComponent>,
    private _snackBar: MatSnackBar,
  ){
    this.loadForm();
  }

  async ngOnInit() {
    if(this.data!=null){
      this.editMode();
      this.isEdit = true;
    }
    else
      this.createMode();

    await this.loadRoles();
  }

  

  editMode(){
    this.titlekey = "Cambiar contraseña de acceso";
  }

  createMode(){
    this.titlekey = "Nueva contraseña de acceso";
  }

  loadForm(){
    this.userForm = this.formBuilder.group({
      name: this.data!=null ? this.data.name : '',
      lastname: this.data!=null ? this.data.lastname : '',
      email: this.data!=null  ? this.data.email : '',
      phone:this.data!=null ? this.data.phone : '',
      code: '',
      operation_code:this.data!=null ? this.data.operation_code : '',
      rol:this.data!=null ? this.data.rol : '',
      img_url:this.data!=null ? this.data.img_url : '',
    });
  }

  openFile(){
    let el: HTMLElement = this.myDiv.nativeElement;
    el.click();
  }

  newImage(input){
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = (e: any) => {
        this.img_url = e.target.result;
        this.selectedFileF = new File([e.target.result], uuidv4(), {type: 'png'});
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  

  closeDialog(){
    this.dialogRef.close();
  }

  async saveEmployee(data){
    if(data.value.name!="" && data.value.lastname!="" && data.value.email!="" && data.value.cellphone!=""){
      let dataSend = {
        id:this.data!=null ? this.data.id : null,
        name:data.value.name,
        lastname:data.value.lastname,
        email:data.value.email,
        cellphone:data.value.cellphone,
        address:"--",
        password:data.value.password,
      }
    }

    
    let formData = new FormData();
    formData.append("archivoDelUsuario", this.selectedFileF);

    let a = this.userForm.value;

    if(this.isEdit){
      a.id = this.data.id
      if(a.rol == -1)
        a.rol = null;
      await this.userService.updateAnUser(a).subscribe((result:any)=>{
        this.openSnackBar("Usuario actualizado exitosamente","Aceptar")
        this.closeDialog()
      },(error:any)=>{
        this.playAudioError();
      });
    }else{
      await this.userService.saveAnUser(a).subscribe((result:any)=>{
        this.openSnackBar("Usuario creado exitosamente","Aceptar")
        this.closeDialog()
      },(error:any)=>{
        this.playAudioError();
      });
    }
    /*
    await this.companyService.uploadFile(formData).subscribe(async (result:any)=>{
      console.log(result[0].Location);
      this.urlIF = result[0].Location;
      console.log("saved 1", this.urlIF)
      await this.uploadImageToAWST();
    },(error:any)=>{
    }*/
  }

  async loadRoles(){
    await this.userService.getAllUsersRoles().subscribe((result:any)=>{
      this.roles = result;
      this.roles.splice(0, 0, {
        "id":-1,
        "name":"Sin rol"
      })
      this.loadUserRole();
    },(error:any)=>{
      console.log("error", error)
    });
  }

  loadUserRole(){
    if(this.data.role)
      this.userForm.controls['rol'].setValue(this.data.role.id);
    else
    this.userForm.controls['rol'].setValue(-1);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  playAudioSuccess(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/success.mp3";
    audio.load();
    audio.play();
  }
  
  playAudioError(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/error.mp3";
    audio.load();
    audio.play();
  }

}
