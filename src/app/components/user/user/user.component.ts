import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../../services/user.service'
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from './edit/dialog.component'
import { DeleteDialogComponent } from '../../components/delete-dialog/delete.component';
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})

export class UserComponent implements OnInit {
  displayedColumns;
  companyInfo;
  backgroundURL;
  nameCompany;

  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private userService: UsersService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
  ){
  }

  async ngOnInit() {
    await this.loadUsers();
  }

  async loadUsers(){
    await this.userService.getAllUsers().subscribe((result:any)=>{
      this.data = result;
    },(error:any)=>{
      console.log("error", error)
    });
  }

  deleteDialog(id){
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        "title":"¿Estás seguro que deseas eliminae este usuario?",
        "buttonText":"Eliminar usuario",
        "contentText":"Esta acción es irreversible"
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if(result){
        await this.userService.deleteUser(id).subscribe((result:any)=>{
          this.openSnackBar("Usuario eliminado exitosamente","Aceptar")
          this.data = this.data.filter(function( obj ) {
            return obj.id !== id;
          });
        },(error:any)=>{
          this.openSnackBar("Error al elmininar el usuario","Aceptar")
        });
      }else{
        this.openSnackBar("Operación cancelada","Aceptar")
      }
    });
  }

  editDialog(data){

    const dialogRef = this.dialog.open(DialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadUsers();
    });
  }

  createDialog(){
    const dialogRef = this.dialog.open(DialogComponent, {
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result)
        this.loadUsers();
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


}