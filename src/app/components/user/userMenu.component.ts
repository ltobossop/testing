import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { UsersService } from '../../services/user.service'
import {MatPaginator} from '@angular/material/paginator';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Router} from "@angular/router"

@Component({
  selector: 'app-user-menu',
  templateUrl: './userMenu.component.html',
  styleUrls: ['./userMenu.component.css'],
})

export class UserMenuComponent implements OnInit {
  displayedColumns;
  companyInfo;
  backgroundURL;
  nameCompany;

  data = [];

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private userService: UsersService,
    public dialog: MatDialog,
    private router: Router,
  ){
  }

  async ngOnInit() {
  }


  returnToHome(){
    this.router.navigate(['/home'])
  }



}