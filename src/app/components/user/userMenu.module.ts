import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Component} from '@angular/core';


import { UserMenuComponent } from './userMenu.component';
import { RoleModule } from './role/role.module';
import { RoleComponent } from './role/role.component';
import { UserComponent } from './user/user.component';
import { ImpresionesLocasComponent } from './impresioneslocas/impresioneslocas.component';
import { UserModule } from './user/user.module';
import {DialogComponent} from './user/edit/dialog.component'
import {EditRoleComponent} from './role/edit/dialog.component'

import { TitleBarModule } from '../components/title-bar/title-bar.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatCardModule} from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { Routes, RouterModule } from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import { DialogModule } from './user/edit/dialog.module';
import { EditRoleModule } from './role/edit/dialog.module';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { DeleteDialogComponent } from '../components/delete-dialog/delete.component';
import {MatRippleModule} from '@angular/material/core';
import {NgxPrintModule} from 'ngx-print';
import { ThermalPrintModule } from 'ng-thermal-print'; //add this line
import { PrintService} from 'ng-thermal-print';

@NgModule({
  declarations: [
    UserMenuComponent,
    RoleComponent,
    UserComponent,
    DeleteDialogComponent,
    ImpresionesLocasComponent
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    MatDividerModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatGridListModule,
    MatTableModule,
    MatTooltipModule,
    MatCardModule,
    TitleBarModule,
    MatTabsModule,
    RouterModule,
    MatMenuModule,
    DialogModule,
    EditRoleModule,
    MaterialFileInputModule,
    MatRippleModule,
    NgxPrintModule,
    ThermalPrintModule
  ],
  exports: [UserMenuComponent],
  bootstrap: [UserMenuComponent],
  entryComponents: [DialogComponent, DeleteDialogComponent, EditRoleComponent],
  providers: [PrintService]

})
export class UserMenuModule { }