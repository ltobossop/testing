import { NgModule }      from '@angular/core';
const UNIT_PRODUCTS =[
  {id:0, name:"Pieza", abbreviation:"PZ"},
  {id:1, name:"Gramo", abbreviation:"GR"},
  {id:2, name:"Kilo", abbreviation:"KG"},
  {id:3, name:"Onza", abbreviation:"OZ"},
  {id:4, name:"Libra", abbreviation:"LB"},
  {id:5, name:"Litro", abbreviation:"LT"},
  {id:6, name:"Mililitro", abbreviation:"ML"},
  {id:7, name:"Galón", abbreviation:"GAL"},
]
