import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
import {environment} from '../config/environment'
import { HttpRequestModel } from "../config/HttpRequest";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(
    private http: HttpClient
  ){
  }

  loadInfoLoggedUser(){
    return sessionStorage.getItem('uT');
  }

  getInfo(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'business/').pipe(
      map(resp => resp.data)
    )
  }
  saveCompany(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'business/', data).pipe(
      map(resp => resp.data)
    )
  }
  updateCompany(data){
    return this.http.put<HttpRequestModel.Response>(environment.apiBase+'business/', data).pipe(
      map(resp => resp.data)
    )
  }

}
