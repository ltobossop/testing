import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
import {environment} from '../config/environment'
import { HttpRequestModel } from "../config/HttpRequest";

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(
    private http: HttpClient
  ){
  }

  getAllInventory(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'inventory/').pipe(
      map(resp => resp.data)
    )
  }
  
  getAllUnits(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'unit/').pipe(
      map(resp => resp.data)
    )
  }
  
  getAllCategories(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'category/').pipe(
      map(resp => resp.data)
    )
  }

  saveItemInventory(item){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'inventory/',item).pipe(
      map(resp => resp.data)
    )
  }
  
  saveProduct(item){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'product/',item).pipe(
      map(resp => resp.data)
    )
  }
  saveProductRecipe(item){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'recipes/',item).pipe(
      map(resp => resp.data)
    )
  }
  
  saveProductModifier(item){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'modifiers/',item).pipe(
      map(resp => resp.data)
    )
  }
  
  saveProductSuggested(item){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'suggested/',item).pipe(
      map(resp => resp.data)
    )
  }


  saveItemEntryInventory(item){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'inventoryMovements/',item).pipe(
      map(resp => resp.data)
    )
  }

  
  deleteItemInventory(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'inventory/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  deleteItemRecipe(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'recipes/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  deleteItemModifier(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'modifiers/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  deleteItemSuggested(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'suggested/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  findItemInventory(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'inventory/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  findItemInventoryMovements(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'inventoryMovements/'+id).pipe(
      map(resp => resp.data)
    )
  }
}
