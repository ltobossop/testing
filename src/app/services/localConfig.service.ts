import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
import {environment} from '../config/environment'
import { HttpRequestModel } from "../config/HttpRequest";

@Injectable({
  providedIn: 'root'
})
export class LocalConfigService {

  theme = null;
  userLogged;

  constructor(
    private http: HttpClient
  ){
  }

  setTheme(theme){
    if(theme!='LIGHT' && theme!='DARK'){
      this.theme = this.theme = "LIGHT";
      return;
    }
    this.theme=theme;
    return;
  }
  
  getTheme(){
    if(this.theme==null){
      this.theme = "LIGHT";
    }
    return this.theme
  }

  setTittle(){
    
  }

  getInfo(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'company/').pipe(
      map(resp => resp.data)
    )
  }
  
  getCover(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'company/cover/').pipe(
      map(resp => resp.data)
    )
  }

}
