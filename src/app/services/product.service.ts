import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
import {environment} from '../config/environment'
import { HttpRequestModel } from "../config/HttpRequest";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private http: HttpClient
  ){
  }

  findProduct(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'product/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  findProductModifiers(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'product/modifiers/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  findProductSuggested(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'product/suggested/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  saveProduct(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'product/',data).pipe(
      map(resp => resp.data)
    )
  }
  updateProduct(data){
    return this.http.put<HttpRequestModel.Response>(environment.apiBase+'product/',data).pipe(
      map(resp => resp.data)
    )
  }
  
  deleteProduct(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'product/'+id).pipe(
      map(resp => resp.data)
    )
  }


  getAllProducts(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'product/').pipe(
      map(resp => resp.data)
    )
  }

  getAllUsersRoles(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'users/roles').pipe(
      map(resp => resp.data)
    )
  }
}
