import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
import {environment} from '../config/environment'
import { HttpRequestModel } from "../config/HttpRequest";

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  constructor(
    private http: HttpClient
  ){
  }

  getAllProviders(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'provider/').pipe(
      map(resp => resp.data)
    )
  }
  
  getAllCategories(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'category/').pipe(
      map(resp => resp.data)
    )
  }
  
  getAllPromotions(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'promotions/').pipe(
      map(resp => resp.data)
    )
  }


  getAProvider(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'provider/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  getACategory(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'category/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  saveAProvider(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'provider/',data).pipe(
      map(resp => resp.data)
    )
  }
  
  saveACategory(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'category/',data).pipe(
      map(resp => resp.data)
    )
  }
  
  saveAPromotion(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'promotions/',data).pipe(
      map(resp => resp.data)
    )
  }
  
  getAPromotion(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'promotions/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  deleteProvider(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'provider/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  deleteCategory(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'category/'+id).pipe(
      map(resp => resp.data)
    )
  }

}
