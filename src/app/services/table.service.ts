import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators'
import {environment} from '../config/environment'
import { HttpRequestModel } from "../config/HttpRequest";

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(
    private http: HttpClient
  ){
  }

  getAnUser(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'users/'+id).pipe(
      map(resp => resp.data)
    )
  }


  getAllUsers(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'users/').pipe(
      map(resp => resp.data)
    )
  }

  getAllUsersRoles(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'users/roles').pipe(
      map(resp => resp.data)
    )
  }
  

  //Tables
  getAllInfo(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'tables/allInfo').pipe(
      map(resp => resp.data)
    )
  }
  
  saveAllTables(table){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'tables', table).pipe(
      map(resp => resp.data)
    )
  }
}
