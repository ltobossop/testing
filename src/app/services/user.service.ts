import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators'
import {environment} from '../config/environment'
import { HttpRequestModel } from "../config/HttpRequest";
import { Observable } from 'rxjs/internal/Observable';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ){
  }

  loadInfoLoggedUser(){
    return sessionStorage.getItem('uT');
  }
  
  setInfoLoggedUser(token){
    return sessionStorage.setItem('uT',token);
  }

  eraseCookie(name) {   
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }
  setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  }
  getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return JSON.parse(c.substring(nameEQ.length,c.length));
    }
    return null;
  }

  getAnUser(id){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'users/'+id).pipe(
      map(resp => resp.data)
    )
  }
  
  saveAnUser(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'users',data).pipe(
      map(resp => resp.data)
      )
    }
  
  updateAnUser(data){
  return this.http.put<HttpRequestModel.Response>(environment.apiBase+'users',data).pipe(
    map(resp => resp.data)
    )
  }
    
  deleteUser(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'users/'+id).pipe(
      map(resp => resp.data)
    )
  }

  getAllUsers(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'users/').pipe(
      map(resp => resp.data)
    )
  }

  getAllUsersRoles(){
    return this.http.get<HttpRequestModel.Response>(environment.apiBase+'users/roles').pipe(
      map(resp => resp.data)
    )
  }
  deleteRole(id){
    return this.http.delete<HttpRequestModel.Response>(environment.apiBase+'roles/'+id).pipe(
      map(resp => resp.data)
    )
  }

  post_login(user): Observable<HttpResponse<any>> {
    const headers:HttpHeaders = this.getAuthorizationHeaders();
    const body = new HttpParams()
      .set('username', user.username)
      .set('password', user.password)
      .set('grant_type', user.grant_type)
    return  this.http.post<any>(environment.apiBase+'oauth2/access_token', body, {
      headers: headers,
      observe: 'response'
    }).pipe(map(response=>{
      return response.body;
    }));
  }

  sendEmailRecoveryPassword(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'users/changePassword', data).pipe(
      map(resp => resp.data)
    )
  }
  

  findByUUIDRecoveryPassword(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'users/findByTokenRecovery', data).pipe(
      map(resp => resp.data)
    )
  }
  
  changePasswordToken(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'users/changePasswordToken', data).pipe(
      map(resp => resp.data)
    )
  }

  getAuthorizationHeaders(application:"sga"|"al" = "sga"):HttpHeaders{
    return (new HttpHeaders({
      'Content-Type':	'application/x-www-form-urlencoded',
      'Authorization': 'Basic '+btoa('Pospay'+":"+'fGx4=yU-j4^jAAjZtV+YTDsm-@R$HAK3') 
    }));
  }


  createRole(data){
    return this.http.post<HttpRequestModel.Response>(environment.apiBase+'roles', data).pipe(
      map(resp => resp.data)
    )
  }
  
  editRole(data){
    return this.http.put<HttpRequestModel.Response>(environment.apiBase+'roles', data).pipe(
      map(resp => resp.data)
    )
  }
}
